package com.fit.chernik.vkoffline.Stuff;

import android.net.Uri;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Loader {
    private static String nameDir = null;
    private static String nameDBfile = null;
    private static String logcat_id = null;

    public Loader() {
    }

    public static String getLI() {
        return logcat_id;
    }

    /**
     * Assert logged data
     *
     * @param nameDir    working directory
     * @param nameDBfile DB file
     * @param logcat_id  tag for logcat
     */
    public static void assertData(String nameDir, String nameDBfile, String logcat_id) {
        Loader.nameDir = nameDir;
        Loader.nameDBfile = nameDBfile;
        Loader.logcat_id = logcat_id;
    }

    /**
     * Get path delim
     *
     * @return path delim
     */
    private static String getDelim() {
        return "/";
    }

    /**
     * Path.join, but API
     *
     * @param start path
     * @param end   right part
     * @return result
     */
    public static File addPath(File start, String end) {
        return new File(
                start.getAbsolutePath() +
                        getDelim() +
                        end
        );
    }

    /**
     * @return file of working directory
     * @throws LoaderError error with working directory
     */
    private static File getExFolder() throws LoaderError {
        if (nameDir == null) {
            throw new LoaderError("Cannot find name of working directory");
        }
        File f = StorageController.rootDir();
        if (!f.exists()) {
            throw new LoaderError("Working directory doesn't exist");
        }
        f = addPath(f, nameDir);
        if (!f.exists()) {
            if (!f.mkdir()) {
                throw new LoaderError("Cannot create working directory");
            }
        }
        if (!f.isDirectory()) {
            throw new LoaderError("Working directory is not a directory");
        }
        return f;
    }

    /**
     * Generic unique file mp3 name
     *
     * @param url url to *.mp3
     * @return file name without .mp3
     */
    private static String genericName(Uri url) {
        return String.format(
                Locale.ENGLISH,
                "%d_%d",
                Calendar.getInstance().getTimeInMillis(),
                url.toString().hashCode()
        );
    }

    /**
     * Get stream to file
     *
     * @param name generic name of file
     * @return stream
     * @throws LoaderError error with create file
     */
    private static FileOutputStream getDestFile(String name) throws LoaderError {
        File dest = addPath(getExFolder(), name + ".mp3");
        if (dest.exists()) {
            throw new LoaderError("Wow! Wait please...");
        }
        try {
            dest.createNewFile();
        } catch (IOException e) {
            throw new LoaderError("Cannot create file");
        }
        if (!dest.canWrite()) {
            throw new LoaderError("Cannot write to file");
        }
        FileOutputStream destStream;
        try {
            destStream = new FileOutputStream(dest);
        } catch (FileNotFoundException e) {
            throw new LoaderError("Wow! Cannot open output stream");
        }
        return destStream;
    }

    private static File ejectName(Uri url) throws LoaderError {
        File f;
        try {
            f = new File(new URI(url.toString()));
        } catch (Throwable e) {
            return null;
        }
        if (addPath(getExFolder(), f.getName()).equals(f)) {
            return f;
        }
        return null;
    }

    /**
     * Save file from VK to phone and return mp3 name without .mp3
     *
     * @param url url to .mp3
     * @return file name
     * @throws LoaderError many variants, look other class methods (all from them had calling)
     */
    static public String url2file(Uri url) throws LoaderError {
        File cache = ejectName(url);
        String name = genericName(url);
        if (cache != null) {
            if (!cache.renameTo(addPath(getExFolder(), name + ".mp3"))) {
                throw new LoaderError("Cannot rename file");
            }
            return name;
        }
        Streamer.download(url, getDestFile(name));
        /*
        BufferedInputStream src;
        FileOutputStream dest = getDestFile(name);
        byte[] buf = new byte[1024 * 1024 * 10];
        int readed = 0;
        int available;
        int res;
        int pos = 0;
        try {
            double lastSize = 0;
            long lastTime = Calendar.getInstance().getTimeInMillis();
            while (true) {
                / *
                available = src.available();
                if (available <= 0) {
                    available = buf.length;
                }
                if (available > buf.length) {
                    available = buf.length;
                }
                * /
                available = buf.length;
                res = src.read(buf, 0, available);
                if (res == -1) {
                    break;
                }
                dest.write(buf, 0, res);
                readed += res;
                double size = (1.0 * readed / 1024);
                long time = Calendar.getInstance().getTimeInMillis();
                double speed = (size - lastSize) * 1000 / (time - lastTime);
                if (size > pos * 100.0) {
                    pos++;
                    Log.i(logcat_id, String.format(
                            "Loaded %f Kbytes with speed %f Kbytes/s",
                            size,
                            speed
                    ));
                }
                lastSize = size;
                lastTime = time;
            }
        } catch (IOException e) {
            throw new LoaderError("Download error at " + (1.0 * readed / 1024) + " Kbyte: " + e.getMessage());
        }
        try {
            src.close();
            dest.close();
        } catch (IOException e) {
            throw new LoaderError("Cannot close streams");
        }
        */
        return name;
    }

    /**
     * Get url to local file
     *
     * @param name id file
     * @return url
     * @throws LoaderError some file error
     */
    static public Uri getUri(String name) throws LoaderError {
        File f = addPath(getExFolder(), name + ".mp3");
        if (!f.exists()) {
            throw new LoaderError("File doesn't exist");
        }
        if (!f.canRead()) {
            throw new LoaderError("No readable file");
        }
        return Uri.fromFile(f);
    }

    static private File getDBfile() throws LoaderError {
        if (nameDBfile == null) {
            throw new LoaderError("Cannot find name of DB file");
        }
        File f = getExFolder();
        f = addPath(f, nameDBfile);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                throw new LoaderError("Cannot create DB");
            }
        }
        if (!f.canRead() || !f.canWrite()) {
            throw new LoaderError("Cannot write into DB");
        }
        return f;
    }

    static private List<String> getDB() throws LoaderError {
        File f = getDBfile();
        BufferedReader br;
        try {
            br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(f)
                    )
            );
        } catch (FileNotFoundException e) {
            throw new LoaderError("Wow! Cannot read DB");
        }
        List<String> lines = new ArrayList<>();
        try {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.length() > 0) {
                    lines.add(line);
                }
            }
            br.close();
        } catch (IOException e) {
            throw new LoaderError("Unsucs read DB");
        }
        return lines;
    }

    private static void saveDB(List<String> lines) throws LoaderError {
        File f = getDBfile();
        FileOutputStream des;
        try {
            des = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            throw new LoaderError("Wow! Cannot write to DB");
        }
        try {
            for (String line : lines) {
                line += "\n";
                des.write(line.getBytes());
            }
            des.close();
        } catch (IOException e) {
            throw new LoaderError("Cannot write to DB");
        }
    }

    /**
     * Save audio string
     *
     * @param audioStr audio string
     * @return position
     * @throws LoaderError some error
     */
    static public int saveAudio(String audioStr) throws LoaderError {
        List<String> lines = getDB();
        int pos = lines.size();
        lines.add(audioStr);
        saveDB(lines);
        return pos;
    }

    /**
     * save audio string at exist pos
     *
     * @param audioStr audio
     * @param pos      position
     * @throws LoaderError some error
     */
    static public void saveAudio(String audioStr, int pos) throws LoaderError {
        List<String> lines = getDB();
        if (lines.size() <= pos || pos < 0) {
            throw new LoaderError("Wrong position");
        }
        lines.set(pos, audioStr);
        saveDB(lines);
    }

    /**
     * Get audio strings
     *
     * @return audio strings
     * @throws LoaderError some error
     */
    static public String[] getAudioStrings() throws LoaderError {
        List<String> lines = getDB();
        return lines.toArray(new String[0]);
    }

    /**
     * clear empty lines
     *
     * @throws LoaderError file error
     */
    static public void cleanDB() throws LoaderError {
        List<String> lines = getDB();
        List<String> newLines = new ArrayList<>();
        for (String line : lines) {
            if (line.length() > 0) {
                newLines.add(line);
            }
        }
        saveDB(newLines);
    }

    /**
     * Remove audio local file
     *
     * @param name audio name
     * @throws LoaderError file error
     */
    static public void removeAudio(String name) throws LoaderError {
        File f = getExFolder();
        f = addPath(f, name + ".mp3");
        if (!f.exists()) {
            throw new LoaderError("File doesn't exist");
        }
        if (!f.delete()) {
            throw new LoaderError("Cannot remove file");
        }
    }

    /**
     * Get all files from working folder
     *
     * @return files
     * @throws LoaderError some error
     */
    public static List<File> getFiles() throws LoaderError {
        File f = getExFolder();
        File[] fs = f.listFiles();
        File dbf = getDBfile();
        List<File> res = new ArrayList<>();
        for (File ff : fs) {
            if (ff.isDirectory() || ff.equals(dbf)) {
                continue;
            }
            res.add(ff);
        }
        return res;
    }

    public static void moveOut(List<File> files) throws LoaderError {
        File f = getExFolder();
        f = addPath(f, genericName(Uri.fromFile(f)));
        if (f.exists()) {
            throw new LoaderError("Wow! Wait please...");
        }
        if (!f.mkdir()) {
            throw new LoaderError("Cannot create moveOut dir");
        }
        for (File cf : files) {
            if (!cf.renameTo(addPath(f, cf.getName()))) {
                throw new LoaderError("Cannot move file");
            }
        }
    }

    public static class LoaderError extends Exception {
        public LoaderError(String s) {
            super(s);
        }
    }
}
