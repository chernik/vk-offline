package com.fit.chernik.vkoffline.Stuff;

import android.net.Uri;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class FilerIntegration {
    static private void copy(InputStream inp, OutputStream out) throws IOException {
        byte[] buf = new byte[1024 * 1024 * 10];
        while (true) {
            int loaded = inp.read(buf);
            if (loaded < 0) {
                break;
            }
            out.write(buf, 0, loaded);
        }
    }

    static private void sendErr(Uri uri) throws URISyntaxException, IOException, FilerError {
        InputStream inp = null;
        OutputStream out = null;
        InputStream res = null;
        try {
            inp = new URI(uri.toString())
                    .toURL()
                    .openStream();
            HttpURLConnection con = (HttpURLConnection) new URL(
                    "http",
                    "localhost",
                    4000,
                    "/upload"
            ).openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("File-Name", "vk-offline-filer.mp3");
            try {
                con.connect();
                out = con.getOutputStream();
            } catch (ConnectException e) {
                throw new FilerError();
            }
            copy(inp, out);
            res = con.getInputStream();
            ByteArrayOutputStream rd = new ByteArrayOutputStream();
            copy(res, rd);
            Log.i(Loader.getLI(), "Filer result > " + rd.toString("UTF-8"));
        } finally {
            if (inp != null) {
                inp.close();
            }
            if (out != null) {
                out.close();
            }
            if (res != null) {
                res.close();
            }
        }
    }

    static public boolean send(Uri uri) throws FilerError {
        try {
            sendErr(uri);
            return true;
        } catch (URISyntaxException | IOException e) {
            Log.e("asdasd", Log.getStackTraceString(e));
            return false;
        }
    }

    public static class FilerError extends Exception {

    }

}
