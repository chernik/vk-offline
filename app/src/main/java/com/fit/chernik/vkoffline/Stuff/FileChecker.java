package com.fit.chernik.vkoffline.Stuff;

import android.net.Uri;
import android.util.Log;

import com.fit.chernik.vkoffline.objects.Audio;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileChecker {
    private static int sizeBuf = 1024 * 512;

    private List<Audio> audios;
    private List<File> wrong;

    public FileChecker(Audio[] savedAudios){
        audios = new ArrayList<>();
        wrong = new ArrayList<>();
        // get files from working directory
        List<File> files;
        try {
            files = Loader.getFiles();
        } catch (Loader.LoaderError loaderError) {
            files = new ArrayList<>();
        }
        // collect normal audios and cache files
        List<Audio> normalAudios = new ArrayList<>();
        List<File> normalAudioFiles = new ArrayList<>();
        for(Audio a : savedAudios){
            File f = audio2file(a);
            if(f != null){ // normal file
                normalAudios.add(a);
                normalAudioFiles.add(f);
                int ind = files.indexOf(f);
                if(ind >= 0) {
                    files.remove(ind);
                }
            } // else already added
        }
        // collect duplicate audios
        List<Audio> duplAudios = new ArrayList<>();
        List<File> duplAudioFiles = new ArrayList<>();
        // check duplicates
        int pos = 0;
        for(File f: files){
            if(pos == 10){
                Log.i(Loader.getLI(), "Check limit");
                break;
            }
            Log.i(Loader.getLI(), String.format("Check file %d from %d, list: %d, wrong: %d",
                    pos + 1, files.size(),
                    audios.size(), wrong.size()
            ));
            pos++;
            try {
                if(checkDupl(f, duplAudios, duplAudioFiles) >= 0){ // check in already duplicates
                    continue; // if find
                }
            } catch (IOException e) { // if error
                continue;
            }
            Log.i(Loader.getLI(), "Deep check...");
            try {
                int res = checkDupl(f, normalAudios, normalAudioFiles); // check in other normal
                if(res >= 0){ // if find
                    // add to already duplicates
                    duplAudios.add(normalAudios.get(res));
                    duplAudioFiles.add(normalAudioFiles.get(res));
                    // remove from normals
                    normalAudios.remove(res);
                    normalAudioFiles.remove(res);
                    continue;
                }
            } catch (IOException ignored) { // if error
                continue;
            }
            // if no copy
            audios.add(Audio.Collector.createFake(
                    null,
                    Uri.fromFile(f),
                    "Unique"
            ));
            wrong.add(f);
        }
    }

    public Audio[] getAudios(){
        return audios.toArray(new Audio[audios.size()]);
    }

    public void moveWrongFiles() throws Loader.LoaderError {
        Loader.moveOut(wrong);
    }

    private File audio2file(Audio a) {
        Uri url;
        File f;
        try {
            url = Audio.Collector.getSavedAudio(a);
            if(url == null){
                throw new Loader.LoaderError("Don't have saved url");
            }
            f = new File(new URI(url.toString()));
        } catch (Loader.LoaderError | URISyntaxException e) {
            Audio.Collector.mark(a, "E: " + e.getMessage());
            try {
                Audio.Collector.removeAudioDoc(a);
            } catch (Loader.LoaderError loaderError) {
                Audio.Collector.mark(a,
                        "E: " + e.getMessage() +
                                ", remove E: " + loaderError.getMessage()
                );
            }
            audios.add(a);
            return null;
        }
        return f;
    }

    private int checkDupl(File cand, List<Audio> origAudio, List<File> origFile) throws IOException {
        for(int i = 0; i < origAudio.size(); i++){
            Log.i(Loader.getLI(), String.format("Compare %d from %d...", i + 1, origAudio.size()));
            try {
                if(comp(origFile.get(i), cand)){
                    Audio orig = origAudio.get(i);
                    if (!audios.contains(orig)) {
                        audios.add(orig);
                    }
                    audios.add(Audio.Collector.createFake(
                            orig,
                            Uri.fromFile(cand),
                            "Duplicate"
                    ));
                    wrong.add(cand);
                    return i;
                }
            } catch (IOException e) {
                audios.add(Audio.Collector.createFake(
                        null,
                        Uri.fromFile(cand),
                        "E: " + e.getMessage()
                ));
                wrong.add(cand);
                throw e;
            }
        }
        return -1;
    }

    private boolean comp(File orig, File cand) throws IOException {
        FileInputStream origS = new FileInputStream(orig);
        FileInputStream candS = new FileInputStream(cand);
        byte[] origB = new byte[sizeBuf];
        byte[] candB = new byte[sizeBuf];
        int origC = 0, candC = 0;
        while(true){
            if(candC == 0) {
                candC = candS.read(candB);
                if(candC < 0){
                    break;
                }
            }
            if(origC == 0) {
                origC = origS.read(origB);
                if(origC < 0){
                    break;
                }
            }
            for(int i = 0; i < origC && i < candC; i++){
                if(origB[i] != candB[i]){
                    return false;
                }
            }
            if(origC < candC){
                System.arraycopy(candB, origC, candB, 0, candC - origC);
                candC -= origC;
                origC = 0;
            }
            else if(candC < origC){
                System.arraycopy(origB, candC, origB, 0, origC - candC);
                origC -= candC;
                candC = 0;
            } else {
                origC = candC = 0;
            }
        }
        return true;
    }

    public boolean isWonderful(){
        return audios.size() == 0;
    }
}
