package com.fit.chernik.vkoffline;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class BrowserActivity extends AppCompatActivity {

    WebView browser;
    String startUrl = "https://m.vk.com/feed";
    String loginUrl = "https://login.vk.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        browser = findViewById(R.id.browser);
        // Needs for load VK page
        browser.getSettings().setJavaScriptEnabled(true);
        if(Build.VERSION.SDK_INT < 19){
            browser.addJavascriptInterface(new CB(), "VKoffline");
        }
        browser.setWebViewClient(new WebViewClient() {
            boolean isOpen = false;
            boolean isRedir = false;

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // Needs for working redirects
                try {
                    if (url.substring(0, loginUrl.length()).equals(loginUrl)) {
                        isRedir = true;
                    }
                } catch(IndexOutOfBoundsException ignore){
                }
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView webView, String url) {
                try {
                    if (
                            url.substring(0, startUrl.length()).equals(startUrl)
                                    && !isOpen && !isRedir) {
                        isOpen = true;
                        if(Build.VERSION.SDK_INT >= 19) {
                            webView.evaluateJavascript("vk.id", new CB());
                        } else {
                            Log.e("Chernik", "Wait...");
                            //webView.addJavascriptInterface(new CB(), "VKoffline");
                            webView.loadUrl("javascript:VKoffline.assertId('' + vk.id);");
                        }
                    }
                    isRedir = false;
                } catch(IndexOutOfBoundsException ignore){

                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        browser.loadUrl(startUrl);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private String getCookie() {
        return CookieManager.getInstance().getCookie(startUrl);
    }

    private class CB implements ValueCallback<String> {
        @Override
        public void onReceiveValue(String value){
            _assertId(value);
        }

        @android.webkit.JavascriptInterface
        public void assertId(final String value){
            _assertId(value);
        }

        public void _assertId(String value){
            String cookie = getCookie();
            Intent res = new Intent();
            long userId;
            try {
                userId = Long.valueOf(value);
                res.putExtra("cookie", cookie);
                res.putExtra("userId", userId);
            } catch (NullPointerException | NumberFormatException e) {
                res.putExtra("error", e.getMessage());
            }
            setResult(RESULT_OK, res);
            finish();
        }
    }
}
