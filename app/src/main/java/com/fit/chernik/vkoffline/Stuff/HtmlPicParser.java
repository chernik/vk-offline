package com.fit.chernik.vkoffline.Stuff;

import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;

import java.io.BufferedInputStream;

public class HtmlPicParser implements Html.ImageGetter {
    private Runnable repaint;
    private int sizeX;
    private int sizeY;

    public HtmlPicParser(Runnable repaint, int sizeX, int sizeY) {
        this.repaint = repaint;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }

    public HtmlPicParser(Runnable repaint){
        this(repaint, -1, -1);
    }

    /**
     * Parser
     *
     * @param source url for pic
     * @return pic
     */
    public Drawable getDrawable(String source) {
        URLDrawable urlDrawable = new URLDrawable();
        ImageGetterAsyncTask asyncTask =
                new ImageGetterAsyncTask(urlDrawable);
        asyncTask.execute(source);
        return urlDrawable;
    }

    private class URLDrawable extends BitmapDrawable {
        // the drawable that you need to set, you could set the initial drawing
        // with the loading image if you need to
        protected Drawable drawable;

        @Override
        public void draw(Canvas canvas) {
            // override the draw to facilitate refresh function later
            if (drawable != null) {
                drawable.draw(canvas);
            }
        }
    }

    private class ImageGetterAsyncTask extends AsyncTask<String, Void, Drawable> {
        URLDrawable urlDrawable;


        public ImageGetterAsyncTask(URLDrawable d) {
            this.urlDrawable = d;
        }

        @Override
        protected Drawable doInBackground(String... params) {
            return fetchDrawable(params[0]);
        }

        @Override
        protected void onPostExecute(Drawable result) {
            urlDrawable.setBounds(
                    0, 0,
                    HtmlPicParser.this.sizeX,
                    HtmlPicParser.this.sizeY
            );
            urlDrawable.drawable = result;
            HtmlPicParser.this.repaint.run();
        }

        private Drawable fetchDrawable(String urlString) {
            BufferedInputStream is;
            try {
                is = Streamer.getStream(Uri.parse(urlString));
            } catch (Loader.LoaderError loaderError) {
                Log.e(Loader.getLI(), "Pic load error: " + loaderError.getMessage());
                return null;
            }
            Drawable drawable = Drawable.createFromStream(is, "src");
            if (sizeX < 0) {
                HtmlPicParser.this.sizeX = drawable.getMinimumWidth();
                HtmlPicParser.this.sizeY = drawable.getMinimumHeight();
            }
            drawable.setBounds(
                    0, 0,
                    HtmlPicParser.this.sizeX,
                    HtmlPicParser.this.sizeY
            );
            return drawable;
        }
    }
}
