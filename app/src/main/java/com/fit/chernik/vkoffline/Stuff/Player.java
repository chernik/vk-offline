package com.fit.chernik.vkoffline.Stuff;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.fit.chernik.vkoffline.objects.Audio;
import com.fit.chernik.vkoffline.objects.ParserError;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class Player {
    private static final Player ourInstance = new Player();
    private Audio[] audios = null;
    private int posNow = -1;
    private MediaPlayer mp = null;
    private Map<String, CB> cbs = new HashMap<>();
    private Context ctx = null;
    private boolean rot = false;
    private Requester requester = null;
    private long timer = 0;

    private Player() {
    }

    public MediaPlayer getMp() {
        return mp;
    }

    public static Player getInstance() {
        return ourInstance;
    }

    public void stopAudio() {
        pauseAudio();
        if (mp != null) {
            mp.release();
            mp = null;
        }
        posNow = -1;
    }

    public Context getContext() {
        return ctx;
    }

    public Requester getRequester() {
        return requester;
    }

    public int getPosNow() {
        if (audios == null || posNow >= audios.length) {
            return -1;
        }
        return posNow;
    }

    public void assertRequester(Requester requester) {
        this.requester = requester;
    }

    public boolean isRot() {
        return rot;
    }

    public void setRot(boolean rot) {
        this.rot = rot;
    }

    public void assertAudios(Audio[] audios) {
        this.audios = audios;
    }

    public void assertContext(Context ctx) {
        this.ctx = ctx;
    }

    private int norma(int pos) {
        if (audios == null) {
            return -1;
        }
        while (pos < 0) {
            pos += audios.length;
        }
        pos = pos % audios.length;
        return pos;
    }

    public void playAudio() {
        playAudio(posNow);
    }

    public void playAudio(int pos) {
        pos = norma(pos);
        getCB("play").run(pos);
        if (pos < 0) {
            getCB("playError").run(pos);
            return;
        }
        if (pos == posNow && mp != null) {
            if (!mp.isPlaying()) {
                mp.start();
            }
            return;
        }
        if (posNow >= 0) {
            getCB("pause").run(posNow); // TODO: pause => playStop
        }
        posNow = pos;
        new MusicLoader(pos).executeOnExecutor(
                AsyncTask.THREAD_POOL_EXECUTOR,
                audios[pos]
        );
    }

    private int playAudioURL(Uri url, int pos) {
        MediaPlayer mpl = MediaPlayer.create(ctx, url); // TODO: context???
        if (pos != posNow) {
            return 1;
        }
        if (mp != null) {
            mp.release();
        }
        if (mpl == null) {
            return 0;
        }
        mp = mpl;
        mp.start();
        Player.getInstance().startTimer();
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mpl) {
                if (isRot()) {
                    playAudio(posNow);
                } else {
                    playAudio(posNow + 1);
                }
            }
        });
        mp.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mpl, int what, int extra) {
                mpl.release();
                mp = null;
                posNow = -1;
                getCB("playError").run(posNow);
                return true;
            }
        });
        return 2;
    }

    private CB getCB(String key) {
        if (cbs.containsKey(key)) {
            return cbs.get(key);
        } else {
            return new CBDef(key);
        }
    }

    public void pauseAudio() {
        if (mp == null) {
            return;
        }
        mp.pause();
        getCB("pause").run(posNow);
    }

    public Audio[] getAudios() {
        return audios;
    }

    public void shuffle() {
        if (audios == null) {
            return;
        }
        Random r = new Random(Calendar.getInstance().getTimeInMillis());
        for (int i = 0; i < audios.length * 10; i++) {
            int s = r.nextInt(audios.length);
            int f = r.nextInt(audios.length);
            if (s == posNow) {
                posNow = f;
            } else if (f == posNow) {
                posNow = s;
            }
            Audio buf = audios[s];
            audios[s] = audios[f];
            audios[f] = buf;
        }
    }

    public int getPlaying() {
        if (mp != null && mp.isPlaying()) {
            return posNow;
        } else {
            return -1;
        }
    }

    public void setCB(String name, CB cb) {
        cbs.put(name, cb);
    }

    public void resetCB() {
        // cbs.clear();
    }

    public void startTimer() {
        timer = Calendar.getInstance().getTimeInMillis();
    }

    public long getTimer() {
        long nowTimer = Calendar.getInstance().getTimeInMillis();
        return nowTimer - timer;
    }

    public void playNext() {
        playAudio(posNow + 1);
    }

    public void playPrev() {
        playAudio(posNow - 1);
    }

    public float getPlayPos() {
        if (mp == null) {
            return -1.0f;
        }
        try {
            return 1.0f * mp.getCurrentPosition() / mp.getDuration();
        } catch (IllegalStateException e) {
            return -1.0f;
        }
    }

    public void setPlayPos(float pos) {
        if (pos < 0 || pos > 1) {
            throw new IllegalArgumentException(String.format("%s is not between 0 & 1", pos));
        }
        if (mp == null) {
            return;
        }
        mp.seekTo((int) (pos * mp.getDuration()));
    }

    public interface CB {
        void run(int pos);
    }

    private static class MusicLoader extends AsyncTask<Audio, Object, Integer> {

        private int pos;

        MusicLoader(int pos) {
            this.pos = pos;
        }

        @Override
        protected Integer doInBackground(Audio... pls) {
            Uri res = null;
            try {
                res = Audio.Collector.getSavedAudio(pls[0]);
                if (res == null) {
                    res = Audio.Collector.getLastURL(pls[0]);
                    if(res == null) {
                        res = Player.getInstance().getRequester()
                                .getAudioUri(pls[0]);
                    }
                }
                publishProgress("Loaded");
            } catch (Requester.RequesterError | ParserError | Loader.LoaderError e) {
                publishProgress(e);
            } catch (Throwable e) {
                publishProgress("Other: " + e.getMessage());
            }
            if (res == null) {
                return 0;
            }
            publishProgress("URL => " + res.toString());
            int ret = Player.getInstance().playAudioURL(res, pos);
            publishProgress(
                    String.format(
                            Locale.ENGLISH,
                            "Sound %d %s",
                            pos,
                            ret == 2 ? "is playing" : "canceled")
            );
            return ret;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
            Object res = values[0];
            if (res instanceof String) {
                Log.i(Loader.getLI(), (String) res);
            } else {
                Log.e(Loader.getLI(), ((Throwable) res).getMessage());
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            switch (result) {
                case 0:
                    Player.getInstance().posNow = -1;
                    Player.getInstance().getCB("playError").run(pos);
                    break;
                case 1:
                    Player.getInstance().getCB("pause").run(pos);
                    break;
                case 2:
                    Player.getInstance().getCB("play").run(pos);
            }
        }
    }

    private class CBDef implements CB {

        String mes;

        CBDef(String mes) {
            this.mes = mes;
        }

        @Override
        public void run(int pos) {
            Log.i(Loader.getLI(), String.format("CB %s pos %d", mes, pos));
        }
    }
}
