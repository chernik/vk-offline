package com.fit.chernik.vkoffline.Stuff;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class PlayerCallback extends BroadcastReceiver {

    public static final int BTN_BACK = 1;
    public static final int BTN_PLAY = 2;
    public static final int BTN_FRONT = 3;

    @Override
    public void onReceive(Context context, Intent intent) {
        int btn = intent.getIntExtra("but", -1);
        Log.i(Loader.getLI(), String.format("Btn %d", btn));
        switch (btn) {
            case BTN_BACK:
                Player.getInstance().playPrev();
                break;
            case BTN_FRONT:
                Player.getInstance().playNext();
                break;
            case BTN_PLAY:
                if (Player.getInstance().getPlaying() == -1) {
                    Player.getInstance().playAudio();
                } else {
                    Player.getInstance().pauseAudio();
                }
                break;
            default:
                Log.e(Loader.getLI(), "Unknown message from player broadcast");
                break;
        }
    }
}
