package com.fit.chernik.vkoffline;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Consumer;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fit.chernik.vkoffline.Stuff.FileChecker;
import com.fit.chernik.vkoffline.Stuff.HtmlPicParser;
import com.fit.chernik.vkoffline.Stuff.JSRunner;
import com.fit.chernik.vkoffline.Stuff.Loader;
import com.fit.chernik.vkoffline.Stuff.Player;
import com.fit.chernik.vkoffline.Stuff.Requester;
import com.fit.chernik.vkoffline.Stuff.StorageController;
import com.fit.chernik.vkoffline.objects.Audio;
import com.fit.chernik.vkoffline.objects.ParserError;
import com.fit.chernik.vkoffline.objects.Playlist;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int BROWSER_RESULT = 1;
    private static final int AUDIO_RESULT = 2;
    private static final int PERM_RESULT = 3;
    private static final int SEARCH_RESULT = 4;
    private static final int SCROLL_RESULT = 5;
    private static final int BG_RESTRICT_SETTINGS_RESULT = 6;

    private static float fullAlpha = 1.0f;
    private static float partAlpha = 0.6f;

    private Requester requester;
    private String logcat_id;
    private TextView logText;
    private ProgressBar progressBar;

    private Audio[] savedAudios = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // -------------------------
        StorageController.updateAll(this);
        logcat_id = getResources().getString(R.string.logcat_id);
        requester = new Requester();
        Button but;
        ImageButton imBut;
        try {
            Audio.Parser.setDecodeURL_JS(
                    JSRunner.fromResource(
                            getResources(),
                            R.raw.url_decoder
                    )
            );
        } catch (IOException e) {
            Log.e(logcat_id, "Cannot load JS decoder");
        } catch (JSRunner.JSError jsError) {
            Log.e(logcat_id, jsError.getMessage());
        }
        Loader.assertData(
                getResources().getString(
                        R.string.offline_fold
                ),
                getResources().getString(
                        R.string.offline_DB
                ),
                getResources().getString(
                        R.string.logcat_id
                )
        );
        startService(new Intent(this, PlayerService.class));
        getPermissions();
        setEvents();
        // -------------------------------------------
        logText = findViewById(R.id.log_text);
        progressBar = findViewById(R.id.progress_bar);
        // -------------------------------------------
        but = findViewById(R.id.btn_browser);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                logText.setText(R.string.text_wait);
                startActv(BrowserActivity.class, BROWSER_RESULT);
            }
        });
        but.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                SharedPreferences pref = getPreferences(MODE_PRIVATE);
                String cookie = pref.getString("cookie_data", null);
                long userId = pref.getLong("cookie_userId", 0);
                if (cookie == null || userId == 0) {
                    Toast.makeText(
                            MainActivity.this,
                            "Cannot find saved cookies",
                            Toast.LENGTH_SHORT
                    ).show();
                    return false;
                }
                requester.setSession(cookie, userId);
                findViewById(R.id.btn_requester)
                        .setEnabled(true);
                findViewById(R.id.btn_main_pl)
                        .setEnabled(true);
                return true;
            }
        });
        // -------------------------------------------
        but = findViewById(R.id.btn_requester);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!v.isEnabled()) {
                    return;
                }
                new PlaylistsLoader().execute();
            }
        });
        // -------------------------------------------
        but = findViewById(R.id.btn_player);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActv(AudioActivity.class, AUDIO_RESULT);
            }
        });
        // -------------------------------------------
        imBut = findViewById(R.id.player_rot);
        imBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean res;
                Player.getInstance().setRot(
                        res = !Player.getInstance().isRot()
                );
                if (res) {
                    v.setAlpha(fullAlpha);
                } else {
                    v.setAlpha(partAlpha);
                }
            }
        });
        imBut.setAlpha(Player.getInstance().isRot() ?
                fullAlpha : partAlpha);
        // -------------------------------------------
        imBut = findViewById(R.id.player_play);
        imBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchPlayStop(true);
                Player.getInstance().playAudio();
            }
        });
        // -------------------------------------------
        imBut = findViewById(R.id.player_pause);
        imBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchPlayStop(false);
                Player.getInstance().pauseAudio();
            }
        });
        // -------------------------------------------
        but = findViewById(R.id.btn_saved_audio);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Player.getInstance().stopAudio();
                Player.getInstance().assertAudios(savedAudios);
                startActv(AudioActivity.class, AUDIO_RESULT);
            }
        });
        // -------------------------------------------
        but = findViewById(R.id.btn_main_pl);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AudiosLoader().execute(Playlist.getMainPlaylist(requester.getUserId()));

            }
        });
        // -------------------------------------------
        but = findViewById(R.id.btn_shuffle);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Player.getInstance().shuffle();
                startActv(AudioActivity.class, AUDIO_RESULT);
            }
        });
        // -------------------------------------------
        but = findViewById(R.id.btn_search);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActv(SearchActivity.class, SEARCH_RESULT);
            }
        });
        // -------------------------------------------
        imBut = findViewById(R.id.player_back);
        imBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Player.getInstance().playPrev();
            }
        });
        // -------------------------------------------
        imBut = findViewById(R.id.player_front);
        imBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Player.getInstance().playNext();
            }
        });
        // -------------------------------------------
        if (Player.getInstance().getPlaying() != -1) {
            switchPlayStop(true);
        }
    }

    private void getPermissions() {
        String[] perms = {
                Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.RECORD_AUDIO
        };
        boolean isPerm = true;
        for (String perm : perms) {
            isPerm = isPerm && PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(
                    this,
                    perm
            );
        }
        if (isPerm) {
            Log.i(logcat_id, "Permissions granted");
            onPerm();
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    perms,
                    PERM_RESULT
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        switch (requestCode) {
            case PERM_RESULT:
                boolean isPerm = permissions.length == grantResults.length;
                for (int res : grantResults) {
                    isPerm = isPerm && PackageManager.PERMISSION_GRANTED == res;
                }
                if (isPerm) {
                    Log.i(logcat_id, "Permissions granted");
                    onPerm();
                } else {
                    Log.e(logcat_id, "Permissions denied");
                }
                break;
            default:
                break;
        }
    }

    private void onPerm() {
        reloadSavedAudios(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(logcat_id, "Background restrict? " + checkBackgroundRestrict());
    }

    private boolean checkBackgroundRestrict() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
            return false;
        }
        ActivityManager activityManager =
                (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        if (!activityManager.isBackgroundRestricted()) {
            return false;
        }
        openRestrictDialog();
        return true;
    }

    private void openRestrictDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setMessage(R.string.restrictDialogContent)
                .setPositiveButton(R.string.restrictDialogOpenSettings, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        openRestrictSettings();
                    }
                })
                .setNegativeButton(R.string.restrictDialogIgnore, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create().show();
    }

    private void openRestrictSettings() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(intent, BG_RESTRICT_SETTINGS_RESULT);
    }

    private void reloadSavedAudios(boolean clear) {
        try {
            if (clear) {
                Loader.cleanDB();
            }
            savedAudios = Audio.Collector.loadAudiosDoc();
        } catch (Loader.LoaderError loaderError) {
            Log.e(logcat_id, "Cannot load saved audios");
        }
        Player p = Player.getInstance();
        p.setCB("play", new Player.CB() {
            @Override
            public void run(int pos) {
                switchPlayStop(true);
            }
        });
        p.setCB("pause", new Player.CB() {
            @Override
            public void run(int pos) {
                switchPlayStop(false);
            }
        });
        p.setCB("playError", new Player.CB() {
            @Override
            public void run(int pos) {
                Toast.makeText(
                        MainActivity.this,
                        R.string.err_loading,
                        Toast.LENGTH_SHORT
                ).show();
                switchPlayStop(false);
            }
        });
    }

    private void setEvents() {
        final Player p = Player.getInstance();
        p.setCB("play", new Player.CB() {
            @Override
            public void run(int pos) {
                if (pos == p.getPosNow()) {
                    switchPlayStop(true);
                }
            }
        });
        p.setCB("pause", new Player.CB() {
            @Override
            public void run(int pos) {
                if (pos == p.getPosNow()) {
                    switchPlayStop(false);
                }
            }
        });
        p.setCB("playError", new Player.CB() {
            @Override
            public void run(int pos) {
                Toast.makeText(
                        MainActivity.this,
                        R.string.err_loading,
                        Toast.LENGTH_SHORT
                ).show();
                if (pos == p.getPosNow()) {
                    switchPlayStop(false);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestedCode, int resultCode, Intent data) {
        logText.setText("");
        switch (requestedCode) {
            case BROWSER_RESULT:
                findViewById(R.id.btn_browser)
                        .setEnabled(true);
                if (resultCode == RESULT_CANCELED) {
                    logText.setText("");
                    return;
                }
                String cookie;
                long userId;
                if (data.hasExtra("error")) {
                    Log.e(logcat_id, data.getStringExtra("error"));
                    logText.setText(String.format(
                            getString(R.string.text_try_again),
                            data.getStringExtra("error")
                    ));
                    return;
                }
                try {
                    cookie = data.getStringExtra("cookie");
                    userId = data.getLongExtra("userId", 0);
                } catch (Throwable e) {
                    Log.e(logcat_id, Log.getStackTraceString(e));
                    return;
                }
                Log.i(logcat_id, String.format("Cookies > %s", cookie));
                Log.i(logcat_id, String.format("User id > %d", userId));
                logText.setText(R.string.text_sucs);
                requester.setSession(cookie, userId);
                SharedPreferences pref = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor ed = pref.edit();
                ed.putLong("cookie_userId", userId);
                ed.putString("cookie_data", cookie);
                ed.apply();
                findViewById(R.id.btn_requester)
                        .setEnabled(true);
                findViewById(R.id.btn_main_pl)
                        .setEnabled(true);
                break;
            case AUDIO_RESULT:
                switchPlayStop(Player.getInstance().getPlaying() >= 0);
                reloadSavedAudios(false);
                setEvents();
                break;
            case SEARCH_RESULT:
                int[] poses;
                try {
                    poses = data.getIntArrayExtra("sounds");
                } catch (NullPointerException e) { // Hasn't result
                    break;
                }
                Audio[] audios = Player.getInstance().getAudios();
                if (poses == null || audios == null) {
                    String err = data.getStringExtra("error");
                    if (err != null) {
                        Toast.makeText(
                                MainActivity.this,
                                err,
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                    break;
                }
                List<Audio> audioList = new ArrayList<>();
                for (int pos : poses) {
                    audioList.add(audios[pos]);
                }
                Player.getInstance().stopAudio();
                Player.getInstance().assertAudios(audioList.toArray(new Audio[poses.length]));
                startActv(AudioActivity.class, AUDIO_RESULT);
            case BG_RESTRICT_SETTINGS_RESULT:
            default:
                break;
        }
    }

    private <A> void showMenu(A[] items, final Consumer<Integer> cb) {
        showMenu(items, cb, logText);
    }

    private <A> void showMenu(A[] items, final Consumer<Integer> cb, View v) {
        final PopupMenu menu = new PopupMenu(this, v);
        for (int i = 0; i < items.length; i++) {
            final int ii = i;
            menu.getMenu().add(
                    i, i, i,
                    Html.fromHtml(
                            items[i].toString(),
                            new HtmlPicParser(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            menu.getMenu().getItem(ii).setVisible(false);
                                            menu.getMenu().getItem(ii).setVisible(true);
                                        }
                                    },
                                    60, 60
                            ),
                            null
                    )
            );
        }
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                cb.accept(item.getItemId());
                return true;
            }
        });
        menu.show();
    }

    private void startActv(Class<?> src, int retCode) {
        Player.getInstance().resetCB();
        startActivityForResult(
                new Intent(this, src),
                retCode
        );
    }

    private void switchPlayStop(boolean state) {
        findViewById(R.id.player_play)
                .setVisibility(state ? View.GONE : View.VISIBLE);
        findViewById(R.id.player_pause)
                .setVisibility(state ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, R.string.menu_check_cache);
        menu.add(0, 3, 0, R.string.menu_go_scroll);
        menu.add(0, 4, 0, R.string.placeForSaving);
        menu.add(0, 2, 0, R.string.menu_close_app);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                stopService(new Intent(MainActivity.this, PlayerService.class));
                finish();
                break;
            case 1:
                new CacheChecker().execute();
                break;
            case 3:
                startActv(ScrollActivity.class, SCROLL_RESULT);
                break;
            case 4:
                chooseStorage();
                break;
            default:
                Log.e(Loader.getLI(), "Wrong menu item code: " + item.getItemId());
                break;
        }
        return true;
    }

    private void chooseStorage() {
        final Pair<String[], StorageController.Choose> variants =
                StorageController.choose(this);
        new AlertDialog.Builder(this)
                .setTitle("Choose your storage")
                .setItems(variants.first, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        variants.second.choice(which);
                        reloadSavedAudios(true);
                    }
                })
                .create().show();
    }

    private class PlaylistsLoader extends AsyncTask<Void, Object, Playlist[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            logText.setText(R.string.text_load_pls);
        }

        @Override
        protected Playlist[] doInBackground(Void... voids) {
            Playlist[] res = null;
            try {
                res = requester.getPlaylists();
                publishProgress("Loaded");
            } catch (Requester.RequesterError | ParserError e) {
                publishProgress(e);
            } catch (Throwable e) {
                publishProgress("Other: " + e.getMessage());
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
            Object res = values[0];
            if (res instanceof String) {
                logText.setText((String) res);
            } else {
                logText.setText(((Throwable) res).getMessage());
            }
        }

        @Override
        protected void onPostExecute(final Playlist[] result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.INVISIBLE);
            if (result == null) {
                return;
            }
            showMenu(
                    result,
                    new Consumer<Integer>() {
                        @Override
                        public void accept(Integer integer) {
                            new AudiosLoader().execute(result[integer]);
                        }
                    }
            );
        }
    }

    private class AudiosLoader extends AsyncTask<Playlist, Object, Audio[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            logText.setText(R.string.text_load_auds);
        }

        @Override
        protected Audio[] doInBackground(Playlist... pls) {
            Audio[] res = null;
            try {
                res = requester.getAudios(pls[0]);
                publishProgress("Loaded");
            } catch (Requester.RequesterError | ParserError e) {
                publishProgress(e);
            } catch (Throwable e) {
                publishProgress("Other: " + e.getMessage());
            }
            if (res == null) {
                return null;
            }
            for (int i = 0; i < res.length; i++) {
                res[i] = Audio.Collector.syncDB(savedAudios, res[i]);
            }
            return res;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
            Object res = values[0];
            if (res instanceof String) {
                logText.setText((String) res);
            } else {
                logText.setText(((Throwable) res).getMessage());
            }
        }

        @Override
        protected void onPostExecute(Audio[] result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.INVISIBLE);
            if (result == null) {
                return;
            }
            logText.setText(R.string.text_wait);
            Player p = Player.getInstance();
            p.stopAudio();
            p.assertAudios(result);
            p.assertRequester(requester);
            Toast.makeText(
                    MainActivity.this,
                    String.format(
                            getString(R.string.text_found_audio),
                            result.length
                    ),
                    Toast.LENGTH_SHORT
            ).show();
            startActv(AudioActivity.class, AUDIO_RESULT);
        }
    }

    private class CacheChecker extends AsyncTask<Void, Object, FileChecker> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            logText.setText(R.string.text_load_cache);
        }

        @Override
        protected FileChecker doInBackground(Void... pls) {
            long t = Calendar.getInstance().getTimeInMillis();
            FileChecker fch = new FileChecker(savedAudios);
            t = Calendar.getInstance().getTimeInMillis() - t;
            Log.i(Loader.getLI(), "Cache checked at " + t + " ms");
            return fch;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
            Object res = values[0];
            if (res instanceof String) {
                logText.setText((String) res);
            } else {
                logText.setText(((Throwable) res).getMessage());
            }
        }

        @Override
        protected void onPostExecute(final FileChecker fch) {
            super.onPostExecute(fch);
            progressBar.setVisibility(View.INVISIBLE);
            logText.setText("");
            if (fch.isWonderful()) {
                Snackbar.make(
                        logText,
                        R.string.text_wond_cache,
                        Snackbar.LENGTH_SHORT
                ).show();
                /*Toast.makeText(
                        MainActivity.this,
                        R.string.text_wond_cache,
                        Toast.LENGTH_SHORT
                ).show();*/
                return;
            }
            showMenu(new String[]{
                    getString(R.string.menu_view_cache),
                    getString(R.string.menu_clr_cache)
            }, new Consumer<Integer>() {
                @Override
                public void accept(Integer integer) {
                    switch (integer) {
                        case 0:
                            Player.getInstance().stopAudio();
                            Player.getInstance().assertAudios(fch.getAudios());
                            startActv(AudioActivity.class, AUDIO_RESULT);
                            break;
                        case 1:
                            try {
                                fch.moveWrongFiles();
                                logText.setText(R.string.text_sucs);
                            } catch (Loader.LoaderError loaderError) {
                                Toast.makeText(
                                        MainActivity.this,
                                        loaderError.getMessage(),
                                        Toast.LENGTH_SHORT
                                ).show();
                            }
                            break;
                    }
                }
            });
        }
    }
}
