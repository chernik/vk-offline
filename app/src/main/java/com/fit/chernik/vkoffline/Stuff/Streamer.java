package com.fit.chernik.vkoffline.Stuff;

import android.net.Uri;
import android.util.Log;

import com.arthenica.mobileffmpeg.FFmpeg;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Streamer {
    /**
     * Connection timeout
     */
    private static final int TIMEOUT = 15000;

    /**
     * Buffer size
     */
    private static final int BUF_SIZE = 1024 * 1024 * 10;

    /**
     * Get stream from url to *.mp3
     *
     * @param url url
     * @return stream
     * @throws Loader.LoaderError error with connection
     */
    public static BufferedInputStream getStream(Uri url) throws Loader.LoaderError {
        URLConnection req;
        try {
            URL urlC = new URL(url.toString());
            req = urlC.openConnection();
            if (req instanceof HttpURLConnection) {
                ((HttpURLConnection) req).setRequestMethod("GET");
            }
        } catch (IOException e) {
            throw new Loader.LoaderError("Cannot create connection");
        }
        req.setDoOutput(false);
        req.setDoInput(true);
        req.setConnectTimeout(TIMEOUT);
        req.setReadTimeout(TIMEOUT);
        BufferedInputStream rd;
        try {
            req.connect();
            rd = new BufferedInputStream(
                    req.getInputStream()
            );
        } catch (IOException e) {
            throw new Loader.LoaderError("Cannot open read: " + e.getMessage());
        }
        return rd;
    }

    private static void transfer(InputStream i, OutputStream o) throws IOException {
        byte[] buf = new byte[BUF_SIZE];
        while (true) {
            int res = i.read(buf);
            if (res < 0) {
                break;
            }
            o.write(buf, 0, res);
        }
        i.close();
    }

    private static void transfer(InputStream i, OutputStream o, String err) throws Loader.LoaderError {
        try {
            transfer(i, o);
        } catch (IOException e) {
            throw new Loader.LoaderError(err);
        }
    }

    /**
     * Download from src to dst
     *
     * @param src src
     * @param dst dst
     */
    public static void download(Uri src, FileOutputStream dst) throws Loader.LoaderError {
        if (src.toString().contains(".mp3")) {
            transfer(getStream(src), dst, "Download mp3 error");
            return;
        }
        if (src.toString().contains(".m3u8")) {
            M3U8 m3u8 = new M3U8(src);
            transfer(m3u8.getResult(), dst, "Get result mp3 error");
            return;
        }
        throw new Loader.LoaderError("Unsupported format");
    }

    static class M3U8 {
        OutputStream bigTs;
        FileInputStream result;
        private String pth;
        private byte[] lastKey = null;

        public M3U8(Uri m3u8) throws Loader.LoaderError {
            String s_m3u8 = m3u8.toString();
            int p = s_m3u8.lastIndexOf("/");
            if (p >= 0) {
                pth = s_m3u8.substring(0, p + 1);
            } else {
                pth = "";
            }
            BufferedReader r_m3u8 = new BufferedReader(new InputStreamReader(getStream(m3u8)));
            File tmp, f_bigTs, f_mp3;
            try {
                tmp = File.createTempFile("temp", Long.toString(System.nanoTime()));
                tmp.delete();
                tmp.mkdir();
                f_bigTs = Loader.addPath(tmp, "all.ts");
                f_mp3 = Loader.addPath(tmp, "result.mp3");
                bigTs = new FileOutputStream(f_bigTs);
            } catch (IOException e) {
                e.printStackTrace();
                throw new Loader.LoaderError("Cannot create temp files");
            }
            matchPlaylist(r_m3u8);
            if (!f_bigTs.exists()) {
                throw new Loader.LoaderError("Big ts not created");
            }
            int r;
            try {
                r = FFmpeg.execute(String.format(
                        "-i \"%s\" -vn -ar 44100 -ac 2 -b:a 320k \"%s\"",
                        f_bigTs.getAbsolutePath(),
                        f_mp3.getAbsolutePath()
                ));
            } catch (Throwable e) {
                e.printStackTrace();
                throw new Loader.LoaderError("FFmpeg error");
            }
            if (r != 0) {
                throw new Loader.LoaderError("FFmpeg cannot format that");
            }
            if (!f_mp3.exists()) {
                throw new Loader.LoaderError("Mp3 not created");
            }
            try {
                f_bigTs.delete();
                result = new FileInputStream(f_mp3);
                f_mp3.delete();
            } catch (FileNotFoundException ignored) {
            }
        }

        public InputStream getResult() {
            return result;
        }

        private void matchPlaylist(BufferedReader r) throws Loader.LoaderError {
            try {
                while (true) {
                    String s = r.readLine();
                    if (s == null) {
                        break;
                    }
                    if (s.contains(".ts")) {
                        matchTs(s);
                        continue;
                    }
                    if (s.contains("URI=")) {
                        s = s.substring(s.indexOf("URI=\"") + 5);
                        s = s.substring(0, s.length() - 1);
                        matchKey(s);
                    }
                }
                r.close();
                bigTs.close();
            } catch (IOException e) {
                throw new Loader.LoaderError("Download m3u8 error");
            }
        }

        private byte[] readBytes(Uri uri, String err) throws Loader.LoaderError {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            InputStream is = getStream(uri);
            transfer(is, os, err);
            return os.toByteArray();
        }

        private byte[] decrypt(byte[] d) {
            try {
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                cipher.init(
                        Cipher.DECRYPT_MODE,
                        new SecretKeySpec(lastKey, "AES"),
                        new IvParameterSpec(new byte[16])
                );
                return cipher.doFinal(d);
            } catch (NoSuchAlgorithmException
                    | NoSuchPaddingException
                    | InvalidAlgorithmParameterException
                    | InvalidKeyException
                    | BadPaddingException
                    | IllegalBlockSizeException e) {
                throw new RuntimeException(e);
            }
        }

        private void matchKey(String s) throws Loader.LoaderError {
            lastKey = readBytes(Uri.parse(s), "Download m3u8 key error");
            Log.i(Loader.getLI(), "m3u8 key > " + new String(lastKey));
        }

        private void matchTs(String s) throws Loader.LoaderError {
            byte[] d = readBytes(Uri.parse(pth + s), "Download ts error");
            if (lastKey != null) {
                d = decrypt(d);
                lastKey = null;
            }
            try {
                bigTs.write(d);
            } catch (IOException e) {
                throw new Loader.LoaderError("Cannot write big ts");
            }
            if (d[0] != (byte) 'G') {
                throw new Loader.LoaderError("Wrong ts format after min check");
            }
            Log.i(Loader.getLI(), "m3u8 ts > " + (d[30] == (byte) 255));
        }
    }
}
