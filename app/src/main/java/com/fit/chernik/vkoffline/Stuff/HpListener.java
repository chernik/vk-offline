package com.fit.chernik.vkoffline.Stuff;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class HpListener extends BroadcastReceiver {

    Context src;
    int lastState = -2;

    public HpListener() {
        this.src = Player.getInstance().getContext();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!Intent.ACTION_HEADSET_PLUG.equals(intent.getAction())) {
            return;
        }
        int state = intent.getIntExtra("state", -1);
        if (state == lastState) {
            return;
        }
        if (state == 0) {
            lastState = state;
            long timer = Player.getInstance().getTimer();
            Log.i(Loader.getLI(), "Timer " + timer);
            if (timer < 1000) { // TODO: yeah, baby
                Log.i(Loader.getLI(), "Fuck this shit");
                return;
            }
            Player.getInstance().pauseAudio();
            Log.i(Loader.getLI(), "Unplugged headphones");
            return;
        }
        if (state == 1) {
            lastState = state;
            Log.i(Loader.getLI(), "Plugged");
            Toast.makeText(
                    src.getApplicationContext(),
                    "Plugged headphones",
                    Toast.LENGTH_SHORT
            ).show();
            return;
        }
        Log.e(Loader.getLI(), String.format("unknown headphones state '%d'", state));
    }
}
