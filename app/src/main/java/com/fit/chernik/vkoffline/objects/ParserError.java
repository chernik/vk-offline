package com.fit.chernik.vkoffline.objects;

public class ParserError extends Exception {
    public ParserError(String s) {
        super(s);
    }
}
