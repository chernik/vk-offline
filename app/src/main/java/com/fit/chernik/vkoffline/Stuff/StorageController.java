package com.fit.chernik.vkoffline.Stuff;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Environment;
import android.util.Pair;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class StorageController {
    private static final int V_ROOT_DIR = 1;
    private static final int V_LOCAL_INTERNAL = 2;
    private static final int V_LOCAL_EXTERNAL = 3;
    private static final int V_ROOT_MUSIC = 4;
    private static final int V_EXTERNAL_ROOT = 5;
    private static final int V_OVER = 6;
    private static final int V_DEFAULT = V_ROOT_DIR;
    private static Map<Integer, File> variants = new HashMap<>();
    private static Integer select = null;

    // select

    private static void updateSelect(Integer select) {
        StorageController.select = select;
    }

    private static void resetSelect() {
        updateSelect(null);
    }

    // reset?
    private static boolean resetSelectIfWrong() {
        if (select == null) return false;
        if (variants.containsKey(select)) return false;
        resetSelect();
        return true;
    }

    public static File rootDir() {
        return variants.get(select == null ? V_DEFAULT : select);
    }

    // variants

    private static boolean updateVariant(int id, File v) {
        if(v == null){
            if(variants.containsKey(id)){
                variants.remove(id);
                return true;
            }
            return false;
        }
        if (variants.containsKey(id) && v.equals(variants.get(id))) {
            return false;
        }
        variants.put(id, v);
        return true;
    }

    private static boolean updateDeepVariants(File[] localVariants) {
        if (localVariants.length == 0) return false;
        boolean res = updateVariant(V_LOCAL_INTERNAL, localVariants[0]);
        if (localVariants.length == 1) return res;
        res = updateVariant(V_LOCAL_EXTERNAL, localVariants[1]) || res;
        if (localVariants.length == 2) return res;
        for (int i = 2; i < localVariants.length; i++) {
            res = updateVariant(V_OVER + i - 2, localVariants[i]) || res;
        }
        return res;
    }

    private static boolean updateExternalRoot() {
        return updateVariant(
                V_EXTERNAL_ROOT, variants.containsKey(V_LOCAL_EXTERNAL)
                ? ejectRoot(variants.get(V_LOCAL_EXTERNAL))
                        : null
        );
    }

    private static File ejectRoot(File file) {
        String path = file.getAbsolutePath();
        int pos = path.indexOf("/Android/");
        if(pos < 0) return null;
        path = path.substring(0, pos);
        return new File(path);
    }

    // Someth changes?
    private static boolean updateVariants(Context context) {
        boolean res = updateVariant(
                V_ROOT_DIR, Environment.getExternalStorageDirectory()
        );
        res = updateVariant(
                V_ROOT_MUSIC, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC)
        ) || res;
        res = updateDeepVariants(
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                        ? context.getExternalFilesDirs(Environment.DIRECTORY_MUSIC)
                        : new File[0]
        ) || res;
        return updateExternalRoot() || res;
    }

    // preferences

    // Is noth to load?
    private static boolean loadPreferences(Activity context) {
        SharedPreferences pref = context.getPreferences(MODE_PRIVATE);
        int select = pref.getInt("storage_select", -1);
        int devicesCount = pref.getInt("storage_devicesCount", -1);
        if (select < 0 || devicesCount < 0 || devicesCount != variants.size()) {
            return true;
        }
        updateSelect(select);
        return false;
    }

    private static void savePreferences(Activity context) {
        SharedPreferences pref = context.getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = pref.edit();
        ed.putInt("storage_select", select == null ? -1 : select);
        ed.putInt("storage_devicesCount", variants.size());
        ed.apply();
    }

    // description

    private static String getDesc(Integer id) {
        switch (id) {
            case V_ROOT_DIR:
                return "Internal root";
            case V_LOCAL_INTERNAL:
                return "Internal local";
            case V_LOCAL_EXTERNAL:
                return "External local";
            case V_ROOT_MUSIC:
                return "Internal root music";
            case V_EXTERNAL_ROOT:
                return "External root (*)";
            case V_OVER:
                return "Someth insane";
            default:
                return "Oh my god";
        }
    }

    // main events

    public static void updateAll(Activity context) {
        int lastSize = variants.size();
        boolean res = updateVariants(context);
        if (lastSize == 0) res = false;
        if (res) {
            if (resetSelectIfWrong() || lastSize != variants.size()) {
                savePreferences(context);
                return;
            }
        }
        if (loadPreferences(context) || resetSelectIfWrong()) {
            savePreferences(context);
        }
    }

    public static Pair<String[], Choose> choose(final Activity context) {
        updateAll(context);
        String[] variants = new String[StorageController.variants.size()];
        final Integer[] ids = StorageController.variants.keySet().toArray(new Integer[0]);
        for (int i = 0; i < variants.length; i++) {
            variants[i] =
                    (ids[i].equals(select) ? "<> " : "")
                            + getDesc(ids[i])
                            + ": " + StorageController.variants.get(ids[i]);
        }
        return new Pair<String[], Choose>(variants, new Choose() {
            @Override
            public void choice(int idx) {
                StorageController.updateSelect(ids[idx]);
                savePreferences(context);
            }
        });
    }

    public interface Choose {
        void choice(int idx);
    }
}
