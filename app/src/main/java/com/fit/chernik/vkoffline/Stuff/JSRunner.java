package com.fit.chernik.vkoffline.Stuff;

import android.content.res.Resources;
import android.util.Log;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JSRunner {

    private Context ctx;
    private Scriptable scope;
    private String code;
    private String nameFile, nameFunc;
    private Function func;

    /**
     * Create new runner from JS code
     * Code must have function 'main', witch called by this runner
     *
     * @param code JS code
     * @param name scope name
     * @throws JSError if compilation or get 'main' is wrong. See {@link JSError}
     */
    public JSRunner(String code, String name) throws JSError {
        this.code = code;
        this.nameFile = name;
        ctx = Context.enter();
        ctx.setOptimizationLevel(-1);
        scope = ctx.initStandardObjects();
        try {
            //ScriptableObject.putProperty(scope, "logg", new Logg());
            ctx.evaluateString(scope, code, name, 1, null);
        } catch (Exception e) {
            throw new JSError(e);
        }
        getFunc("main");
    }

    /**
     * Create runner with default scope name
     *
     * @param code JS code
     * @throws JSError Look {@link JSRunner#JSRunner(String, String)}
     */
    public JSRunner(String code) throws JSError {
        this(code, "<noname>");
    }

    private JSRunner() {
    }

    /**
     * Get runner from input stream. Stream will be close.
     *
     * @param is   input stream
     * @param name name of scope
     * @return runner
     * @throws IOException input stream read error
     * @throws JSError     from {@link #JSRunner(String, String)}
     */
    public static JSRunner fromStream(InputStream is, String name) throws IOException, JSError {
        BufferedReader bs = new BufferedReader(
                new InputStreamReader(
                        is
                )
        );
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = bs.readLine()) != null) {
            sb.append(line).append("\n");
        }
        bs.close();
        return new JSRunner(sb.toString(), name);
    }

    /**
     * {@link JSRunner#fromStream(InputStream, String)} with default name
     *
     * @param is stream
     * @return runner
     * @throws IOException from {@link JSRunner#fromStream(InputStream, String)}
     * @throws JSError     from {@link JSRunner#fromStream(InputStream, String)}
     */
    public static JSRunner fromStream(InputStream is) throws IOException, JSError {
        return fromStream(is, "<stramed noname>");
    }

    /**
     * load runner from resource
     *
     * @param res resources (may be getResources())
     * @param id  id of resouorce (may be R.raw.*)
     * @return runner
     * @throws IOException from {@link JSRunner#fromStream(InputStream, String)}
     * @throws JSError     from {@link JSRunner#fromStream(InputStream, String)}
     */
    public static JSRunner fromResource(Resources res, int id) throws IOException, JSError {
        return fromStream(
                res.openRawResource(id),
                res.getResourceName(id)
        );
    }

    public JSRunner getAnotherFunc(String name) throws JSError {
        JSRunner runner = new JSRunner();
        runner.code = code;
        runner.ctx = ctx;
        runner.scope = scope;
        runner.nameFile = nameFile;
        runner.getFunc(name);
        return runner;
    }

    private void getFunc(String name) throws JSError {
        Object func = scope.get(name, scope);
        if (func instanceof Function) {
            this.func = (Function) func;
            this.nameFunc = name;
        } else {
            throw new JSError("Cannot get function '" + name + "'");
        }
    }

    private Object cl(final Object[] args) throws JSError {
        Context ctx = Context.enter();
        try {
            return func.call(ctx, scope, scope, args);
        } catch (Throwable e) {
            throw new JSError(e);
        }
    }

    /**
     * Call runner
     * Ready to ClassCastException
     *
     * @param args arguments of JS function
     * @param <T>  return type
     * @return result of function
     */
    public <T> T call(Object... args) {
        try {
            return (T) cl(args);
        } catch (JSError jsError) {
            return (T) jsError;
        }
    }

    /**
     * Call runner
     *
     * @param returnType class of return type
     * @param args       arguments of JS function
     * @param <T>        return type
     * @return result of function
     * @throws JSError error in cast
     */
    public <T> T call(Class<?> returnType, Object... args) throws JSError {
        Object res = cl(args);
        if (returnType.isAssignableFrom(res.getClass())) {
            return (T) returnType.cast(res);
        } else {
            throw new JSError("" +
                    "Cannot cast to " +
                    returnType.getName() +
                    " from " +
                    res.getClass().getName()
            );
        }
    }

    public class JSError extends Exception {
        public JSError(Throwable s) {
            super(s);
        }

        public JSError(String s) {
            super(s);
        }
    }

    public class Logg {
        public void log(String s){
            Log.d("Chernik", s);
        }
    }
}
