package com.fit.chernik.vkoffline;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.fit.chernik.vkoffline.Stuff.CallListener;
import com.fit.chernik.vkoffline.Stuff.HpListener;
import com.fit.chernik.vkoffline.Stuff.Loader;
import com.fit.chernik.vkoffline.Stuff.LoopTimer;
import com.fit.chernik.vkoffline.Stuff.Player;

import java.util.Timer;

public class PlayerService extends Service {
    private HpListener recv;
    //private CallListener callRecv;
    private Timer timer;

    public void onCreate() {
        super.onCreate();
        Player.getInstance().assertContext(this);
        recv = new HpListener();
        registerReceiver(
                recv,
                new IntentFilter(Intent.ACTION_HEADSET_PLUG)
        );
        /*
        callRecv = new CallListener();
        registerReceiver(
                callRecv,
                new IntentFilter()
        );
        */
        timer = new Timer();
        timer.scheduleAtFixedRate(new LoopTimer(), 0, 500);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i(Loader.getLI(), "Closed");
        unregisterReceiver(recv);
        timer.cancel();
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null) {
            throw new IllegalStateException("Cannot find notification manager");
        }
        notificationManager.cancel(LoopTimer.NOTIF_PLAYER);
        // stopForeground(LoopTimer.NOTIF_PLAYER); // TODO: from API 24
        Player.getInstance().stopAudio();
    }
}
