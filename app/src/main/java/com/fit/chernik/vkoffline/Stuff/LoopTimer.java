package com.fit.chernik.vkoffline.Stuff;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.fit.chernik.vkoffline.AudioActivity;
import com.fit.chernik.vkoffline.R;

import java.util.TimerTask;

public class LoopTimer extends TimerTask {
    public static final int NOTIF_PLAYER = 1;
    private static final int MAX_DUR = 1000;
    private CB cb = null;
    private String oldContentText = null;
    private String chId = null;

    @Override
    public void run() {
        // Get context
        Context ctx = Player.getInstance().getContext();
        if (ctx == null) {
            return;
        }
        // Get notifications
        NotificationManager notificationManager =
                (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null) {
            throw new IllegalStateException("Cannot find notification manager");
        }
        // Get data from player
        Player p = Player.getInstance();
        int playPausePic = android.R.drawable.ic_media_play;
        String contentText;
        int progress = 0;
        boolean indet = true;
        if (p.getPosNow() < 0) {
            contentText = "<empty>";
        } else {
            contentText = p.getAudios()[p.getPosNow()].toString();
            if (p.getPlayPos() < 0) {
                progress = 0;
                indet = true;
            } else {
                progress = (int) (p.getPlayPos() * MAX_DUR);
                indet = false;
                if (p.getPlaying() >= 0) {
                    playPausePic = android.R.drawable.ic_media_pause;
                }
            }
            if (cb != null) {
                cb.run(p.getPlayPos());
            }
        }
        // Start build
        RemoteViews remoteViews = new RemoteViews(
                ctx.getPackageName(),
                R.layout.layout_notify
        );
        remoteViews.setTextViewText(R.id.n_text, contentText);
        remoteViews.setImageViewResource(R.id.n_play, playPausePic);
        remoteViews.setProgressBar(R.id.n_progress, MAX_DUR, progress, indet);
        remoteViews.setOnClickPendingIntent(R.id.n_text, PendingIntent.getActivity(
                ctx,
                0,
                new Intent(ctx, AudioActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT
        ));
        remoteViews.setOnClickPendingIntent(R.id.n_back, PendingIntent.getBroadcast(
                ctx,
                PlayerCallback.BTN_BACK,
                new Intent(ctx, PlayerCallback.class)
                        .putExtra("but", PlayerCallback.BTN_BACK),
                PendingIntent.FLAG_UPDATE_CURRENT
        ));
        remoteViews.setOnClickPendingIntent(R.id.n_play, PendingIntent.getBroadcast(
                ctx,
                PlayerCallback.BTN_PLAY,
                new Intent(ctx, PlayerCallback.class)
                        .putExtra("but", PlayerCallback.BTN_PLAY),
                PendingIntent.FLAG_UPDATE_CURRENT
        ));
        remoteViews.setOnClickPendingIntent(R.id.n_front, PendingIntent.getBroadcast(
                ctx,
                PlayerCallback.BTN_FRONT,
                new Intent(ctx, PlayerCallback.class)
                        .putExtra("but", PlayerCallback.BTN_FRONT),
                PendingIntent.FLAG_UPDATE_CURRENT
        ));
        if(chId == null){
            chId = "com.fit.chernik.vkoffline";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                NotificationChannel ch = new NotificationChannel(
                        chId,
                        "vk-offline channel",
                        NotificationManager.IMPORTANCE_NONE
                );
                ch.setLightColor(Color.BLUE);
                ch.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
                notificationManager.createNotificationChannel(ch);
            }
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(ctx, chId)
                .setSmallIcon(android.R.drawable.ic_menu_slideshow)
                .setCustomContentView(remoteViews)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                //.setProgress(MAX_DUR, progress, indet)
                .setWhen(0)
                .setPriority(Notification.PRIORITY_MAX);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder
                    .setCategory(Notification.CATEGORY_SERVICE)
                    .setOngoing(true);
        }
        Notification n = builder.build();
        n.flags |= Notification.FLAG_NO_CLEAR;
        if (ctx instanceof Service) {
            ((Service) ctx).startForeground(NOTIF_PLAYER, n);
        } else {
            notificationManager.notify(NOTIF_PLAYER, n);
        }
    }

    public void setCb(CB cb) {
        this.cb = cb;
    }

    public interface CB {
        void run(float pos);
    }
}
