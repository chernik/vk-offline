package com.fit.chernik.vkoffline.Stuff;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telecom.Call;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import static android.telephony.TelephonyManager.*;

public class CallListener extends BroadcastReceiver {

    public CallListener(){
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        String state = intent.getStringExtra(EXTRA_STATE);
        if(TelephonyManager.EXTRA_STATE_RINGING.equals(state)){
            Player.getInstance().pauseAudio();
            return;
        }
        if(TelephonyManager.EXTRA_STATE_IDLE.equals(state)){
            Toast.makeText(Player.getInstance().getContext(), "Call out!", Toast.LENGTH_SHORT).show();
        }
    }
}
