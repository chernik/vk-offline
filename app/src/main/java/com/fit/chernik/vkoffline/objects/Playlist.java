package com.fit.chernik.vkoffline.objects;

import android.text.Html;
import android.util.Log;

import com.fit.chernik.vkoffline.Stuff.Loader;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Playlist {
    private long id;
    private long ownerId;
    private String accessHash;
    private String urlPic;
    private String name;
    private int audioCount = -1;

    private Playlist() {
    }

    static public Playlist getMainPlaylist(long ownerId) {
        Playlist p = new Playlist();
        p.id = -1;
        p.ownerId = ownerId;
        p.accessHash = "";
        p.urlPic = null;
        p.name = "~ All songs ~";
        return p;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        String name = Html.escapeHtml(getName());
        if (urlPic == null) {
            return name;
        } else {
            Log.i(Loader.getLI(), urlPic);
            return String.format(
                    "<img height=\"100%%\" src=\"%s\"/> %s",
                    urlPic,
                    name
            );
        }
    }

    public Map<String, String> getParamsForReq() {
        return Collector.getParamsForReq(this);
    }

    public int getAudioCount() {
        return audioCount;
    }

    public static class Parser {
        private static Elements parseData(String[] data) throws ParserError {
            JSONObject obj;
            try {
                obj = new JSONObject(data[1]);
            } catch (JSONException e) {
                throw new ParserError("Playlist.Parser - cannot parse JSON payload");
            }
            String html;
            try {
                html = obj.getJSONArray("payload").getJSONArray(1).getString(0);
            } catch (JSONException e) {
                throw new ParserError("Playlist.Parser - cannot find JSON payload");
            }
            Document doc = Jsoup.parse(html);
            try {
                return doc
                        .getElementsByClass("audio_page_block__playlists_items")
                        .get(0)
                        .children();
            } catch (IndexOutOfBoundsException e) {
                throw new ParserError("Playlist.Parser - cannot parse html");
            }
        }

        private static Playlist parseMap(Element el) {
            String[] parseArr;
            parseArr = el
                    .getElementsByTag("A")
                    .get(0)
                    .attr("style")
                    .split(";", -1);
            String urlPic = null;
            for (String aParseArr : parseArr) {
                if (aParseArr.split(":", -1)[0].contains("background-image")) {
                    urlPic = aParseArr.split(":", 2)[1];
                    break;
                }
            }
            if (urlPic != null) {
                urlPic = urlPic
                        .split("url[(][\"\']", -1)[1]
                        .split("[\"\'][)]", -1)[0];
            }
            String name = el
                    .getElementsByTag("A")
                    .get(1)
                    .html();
            String buf = el
                    .getElementsByTag("A")
                    .get(0)
                    .attr("href");
            parseArr = buf
                    .split("music/")[1]
                    .split("/")[1]
                    .split("_");
            String accessHash = "";
            if (parseArr.length == 3) {
                accessHash = parseArr[2];
            }
            long id = Long.valueOf(parseArr[1]);
            long ownerId = Long.valueOf(parseArr[0]);
            Playlist p = new Playlist();
            p.accessHash = accessHash;
            p.id = id;
            p.name = name;
            p.ownerId = ownerId;
            p.urlPic = urlPic;
            return p;
        }

        public static Playlist[] parse(String[] data) throws ParserError {
            Elements els = parseData(data);
            List<Playlist> arr = new ArrayList<>();
            for (Element el : els) {
                arr.add(parseMap(el));
            }
            return arr.toArray(new Playlist[arr.size()]);
        }

        public static void attachInfo(JSONObject obj, Playlist playlist) throws ParserError {
            try {
                playlist.audioCount = obj.getInt("totalCount");
            } catch (JSONException e) {
                throw new ParserError("Playlist.Parser - Cannot get playlist info");
            }
        }
    }

    private static class Collector {
        public static Map<String, String> getParamsForReq(Playlist playlist) {
            Map<String, String> params = new HashMap<>();
            params.put("access_hash", playlist.accessHash);
            params.put("owner_id", String.valueOf(playlist.ownerId));
            params.put("playlist_id", String.valueOf(playlist.id));
            return params;
        }
    }
}
