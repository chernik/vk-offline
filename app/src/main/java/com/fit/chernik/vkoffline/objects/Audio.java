package com.fit.chernik.vkoffline.objects;

import android.net.Uri;
import android.text.Html;
import android.util.Log;

import com.fit.chernik.vkoffline.Stuff.JSRunner;
import com.fit.chernik.vkoffline.Stuff.Loader;
import com.fit.chernik.vkoffline.Stuff.Requester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Audio {
    private String author;
    private String name;
    private String mark = null;
    private Integer[] codes;
    private String[] urlPics;
    private String[] urlHash;
    private long fromUser = 0;
    private String savedURL = null;
    private int posDB = -1;
    private String lyrics = null;

    private String tag;
    private Uri lastURL = null;

    private Audio() {
    }

    @Override
    public int hashCode() {
        int res = 0;
        for (Integer code : codes) {
            res += code.hashCode();
        }
        return res;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Audio)) {
            return false;
        }
        Audio aud = (Audio) o;
        if (aud.hashCode() != hashCode()) {
            return false;
        }
        if (aud.codes.length != codes.length) {
            return false;
        }
        boolean res = true;
        for (int i = 0; i < codes.length; i++) {
            res = res && codes[i].equals(aud.codes[i]);
        }
        return res;
    }

    public boolean hasPic(){
        return urlPics.length > 0;
    }

    private Uri getLastURL() {
        return lastURL;
    }

    public boolean hasText() {
        return codes[2] != 0;
    }

    @Override
    public String toString() {
        if(mark == null) {
            return String.format("%s - %s", author, name);
        } else {
            return String.format("[%s] %s - %s", mark, author, name);
        }
    }

    public String getStrForURL() throws ParserError {
        return Collector.getStrForURL(this);
    }

    public boolean isSaved() {
        return savedURL != null;
    }

    public void setFromUser(long fromUser) {
        this.fromUser = fromUser;
    }

    public String getText() {
        return lyrics;
    }

    public static class Parser {

        static private JSRunner decodeURL_JS = null;

        static public void setDecodeURL_JS(JSRunner runner) {
            decodeURL_JS = runner;
        }

        static private String getFirstJSON(String[] data) throws ParserError {
            return data[1];
        }

        static private JSONObject parseData(String[] data) throws ParserError {
            String str = getFirstJSON(data);
            JSONObject obj;
            try {
                obj = new JSONObject(str);
            } catch (JSONException e) {
                throw new ParserError("Audio.Parser - cannot parse JSON");
            }
            try {
                return obj.getJSONArray("payload").getJSONArray(1).getJSONObject(0);
            } catch (JSONException e) {
                throw new ParserError("Audio.Parser - cannot parse 'payload' field from JSON");
            }
        }

        static private Audio parseMap(JSONArray arr) throws ParserError {
            Audio audio = new Audio();
            audio.urlPics = new String[0];
            try {
                audio.author = Html.fromHtml(arr.getString(4)).toString();
                audio.codes = new Integer[]{
                        arr.getInt(0),
                        arr.getInt(1),
                        arr.getInt(9)
                };
                audio.name = Html.fromHtml(arr.getString(3)).toString();
                audio.tag = Html.fromHtml(arr.getString(16)).toString();
                audio.urlHash = arr.getString(13).split("/", -1);
                String urlPics = arr.getString(14);
                if (!urlPics.equals("")) {
                    audio.urlPics = urlPics.split(",", -1);
                }
            } catch (JSONException e) {
                throw new ParserError("Audio.Parser - Cannot get data from JSON.list[n][m]");
            }
            return audio;
        }

        static private String decodeURL(String url, long userId) throws ParserError {
            if (decodeURL_JS == null) {
                return url;
            } else {
                try {
                    return decodeURL_JS.call(String.class, url, userId);
                } catch (JSRunner.JSError jsError) {
                    throw new ParserError("Cannot run JS decoder: " + jsError.getMessage());
                }
            }
        }

        static public Audio[] parse(String[] data, Playlist playlist) throws ParserError {
            JSONObject obj = parseData(data);
            Playlist.Parser.attachInfo(obj, playlist);
            JSONArray arr;
            try {
                arr = obj.getJSONArray("list");
            } catch (JSONException e) {
                throw new ParserError("Audio.Parser - cannot get field 'list' from JSON");
            }
            List<Audio> audios = new ArrayList<>();
            JSONArray subArr;
            try {
                for (int i = 0; i < arr.length(); i++) {
                    subArr = arr.getJSONArray(i);
                    audios.add(parseMap(subArr));
                }
            } catch (JSONException e) {
                throw new ParserError("Audio.Parser - JSON.list[i] is not an array");
            }
            return audios.toArray(new Audio[audios.size()]);
        }

        static public Uri attachURL(String[] data, Audio audio) throws ParserError {
            JSONArray arr;
            try {
                JSONObject obj = new JSONObject(getFirstJSON(data));
                arr = obj.getJSONArray("payload").getJSONArray(1).getJSONArray(0);
            } catch (JSONException e) {
                throw new ParserError("Audio.Parser - cannot get JSON array from 'payload'");
            }
            String url;
            try {
                url = arr.getJSONArray(0).getString(2);
            } catch (JSONException e) {
                throw new ParserError("Audio.Parser - cannot get url from JSON array");
            }
            url = decodeURL(url, audio.fromUser);
            try {
                new URL(url);
            } catch (MalformedURLException e) {
                throw new ParserError("Audio.Parser - invalid audio URL");
            }
            Uri realUrl = Uri.parse(url);
            audio.lastURL = realUrl;
            return realUrl;
        }

        static public String attachLyrics(String[] data, Audio audio) throws ParserError {
            String res;
            try {
                JSONObject obj = new JSONObject(getFirstJSON(data));
                res = obj.getJSONArray("payload").getJSONArray(1).getString(0);
            } catch (JSONException e) {
                throw new ParserError("Audio.Parser - cannot eject lyrics");
            }
            audio.lyrics = res;
            return res;
        }
    }

    public static class Collector {
        public static String getStrForURL(Audio audio) throws ParserError {
            if (audio.urlHash.length < 6 || audio.urlHash[2].equals("") || audio.urlHash[5].equals("")) {
                throw new ParserError("Audio.Collector - cannot get urlHash[2]");
            }
            return "" +
                    String.valueOf(audio.codes[1]) + "_" +
                    String.valueOf(audio.codes[0]) + "_" +
                    audio.urlHash[2] + "_" + audio.urlHash[5];
        }

        public static Map<String, String> getParamsForLyrics(Audio audio) {
            Map<String, String> res = new HashMap<>();
            res.put("aid", String.format(
                    Locale.ENGLISH,
                    "%d_%d",
                    audio.codes[0],
                    audio.codes[1]
            ));
            res.put("lid", String.valueOf(audio.codes[2]));
            return res;
        }

        static public boolean saveAudio(Audio audio) throws Loader.LoaderError {
            Uri url = audio.getLastURL();
            if (url == null) {
                return false;
            }
            audio.savedURL = Loader.url2file(url);
            Log.i("Chernik", String.format("File <%s>", audio.savedURL));
            saveAudioLoc(audio);
            return true;
        }

        static public Uri getSavedAudio(Audio audio) throws Loader.LoaderError {
            String name = audio.savedURL;
            if (name == null) {
                return null;
            }
            return Loader.getUri(name);
        }

        static private <T> String join(T[] els, String delim) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < els.length; i++) {
                if (i > 0) {
                    sb.append(delim);
                }
                sb.append(els[i]);
            }
            return sb.toString();
        }

        static private String toFileString(Audio audio) {
            if (audio.savedURL == null) {
                return null;
            }
            String[] els = new String[]{
                    Requester.encodeURL(audio.author),
                    Requester.encodeURL(audio.name),
                    join(audio.codes, "й"),
                    join(audio.urlPics, "й"),
                    join(audio.urlHash, "й"),
                    String.valueOf(audio.fromUser),
                    audio.savedURL,
                    audio.lyrics == null ? "э" : Requester.encodeURL(audio.lyrics),
                    Requester.encodeURL(audio.tag)
            };
            return join(els, "ё");
        }

        static private Audio fromFileString(String line, int pos) throws ParserError {
            String lines[] = line.split("ё", -1);
            Audio audio = new Audio();
            try {
                audio.author = Requester.decodeURL(lines[0]);
                audio.name = Requester.decodeURL(lines[1]);
                String[] codes = lines[2].split("й", -1);
                audio.codes = new Integer[codes.length];
                for (int i = 0; i < codes.length; i++) {
                    audio.codes[i] = Integer.valueOf(codes[i]);
                }
                audio.urlPics = lines[3].split("й", -1);
                if (audio.urlPics.length == 1 && audio.urlPics[0].equals("")) {
                    audio.urlPics = new String[0];
                }
                audio.urlHash = lines[4].split("й", -1);
                audio.fromUser = Integer.valueOf(lines[5]);
                audio.savedURL = lines[6];
                audio.lyrics = lines[7].equals("э") ? null : Requester.decodeURL(lines[7]);
                audio.tag = Requester.decodeURL(lines[8]);
                audio.posDB = pos;
            } catch(IndexOutOfBoundsException e){
                throw new ParserError(String.format(
                        "Cannot parse audio\n" +
                                "In code at line #%d of data line #%d\n" +
                                "%s\n" +
                                "> %s",
                        e.getStackTrace()[0].getLineNumber(),
                        pos,
                        e.getMessage(),
                        line
                ));
            }
            return audio;
        }

        static public void saveAudioLoc(Audio audio) throws Loader.LoaderError {
            if (audio.posDB == -1) {
                audio.posDB = Loader.saveAudio(
                        toFileString(audio)
                );
            } else {
                Loader.saveAudio(
                        toFileString(audio),
                        audio.posDB
                );
            }
        }

        static public Audio[] loadAudiosDoc() throws Loader.LoaderError {
            String[] lines = Loader.getAudioStrings();
            Audio[] audios = new Audio[lines.length];
            int delta = 0;
            for (int i = 0; i < lines.length; i++) {
                try {
                    audios[i - delta] = fromFileString(lines[lines.length - i - 1], lines.length - i - 1);
                } catch (ParserError parserError) {
                    Log.e(Loader.getLI(), parserError.getMessage());
                    delta++;
                }
            }
            audios = Arrays.copyOfRange(audios, 0, audios.length - delta);
            return audios;
        }

        static public Audio syncDB(Audio[] audios, Audio audio) {
            int pos = -1;
            for (int i = 0; i < audios.length; i++) {
                if (audios[i].equals(audio)) {
                    pos = i;
                    break;
                }
            }
            if (pos == -1) {
                return audio;
            }
            return audios[pos];
        }

        static public void removeAudioDoc(Audio audio) throws Loader.LoaderError {
            if (audio.savedURL == null) {
                return;
            }
            Loader.saveAudio("", audio.posDB);
            Loader.removeAudio(audio.savedURL);
            audio.savedURL = null;
            audio.posDB = -1;
            audio.lyrics = null;
        }

        static public String getPicsAsHtml(Audio audio){
            if(audio.urlPics.length == 0){
                return "<i>No pics!</i>";
            }
            StringBuilder sb = new StringBuilder();
            for(int pos = audio.urlPics.length-1; pos >= 0; pos--){
                sb.append(
                        String.format(
                                "<img src=\"%s\"/><br>",
                                audio.urlPics[pos]
                        )
                );
            }
            return sb.toString();
        }

        static public void mark(Audio audio, String mark){
            audio.mark = mark;
        }

        static public Audio createFake(Audio base, Uri url, String mark) {
            Audio a = new Audio();
            a.posDB = -1;
            a.mark = mark;
            a.lastURL = url;
            if(base == null){
                a.name = a.author = "???";
            } else {
                a.name = base.name;
                a.author = base.author;
            }
            a.codes = new Integer[3];
            a.codes[0] = a.codes[1] = a.codes[2] = 0;
            a.urlPics = new String[0];
            a.urlHash = new String[0];
            a.tag = "";
            return a;
        }

        static public Uri getLastURL(Audio audio){
            return audio.getLastURL();
        }
    }
}
