package com.fit.chernik.vkoffline;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.Consumer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fit.chernik.vkoffline.Stuff.FilerIntegration;
import com.fit.chernik.vkoffline.Stuff.HtmlPicParser;
import com.fit.chernik.vkoffline.Stuff.Loader;
import com.fit.chernik.vkoffline.Stuff.Player;
import com.fit.chernik.vkoffline.Stuff.Requester;
import com.fit.chernik.vkoffline.objects.Audio;
import com.fit.chernik.vkoffline.objects.ParserError;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AudioActivity extends AppCompatActivity {

    private List<TableLayout> ltLines; // GUI lines
    private TableLayout tbLines; // GUI container
    private LineClicker clicker;
    private AlertDialog dlg = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);
        // --------------------------------------------
        ltLines = new ArrayList<>();
        tbLines = findViewById(R.id.audio_list);
        clicker = new ListClicker(); // Audio list clicker
        if (Player.getInstance().getAudios() == null) {
            Toast.makeText(
                    getApplicationContext(),
                    getResources().getText(R.string.err_empty_playlist),
                    Toast.LENGTH_SHORT
            ).show();
            finish();
            return;
        }
        // --------------------------------------------------------
        ScrollView scr = findViewById(R.id.scroll_audio);
        scr.setOnTouchListener(new View.OnTouchListener() {
            long timer = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int scrollPos = v.getScrollY();
                int introHeight = tbLines.getHeight();
                int screenHeight = v.getHeight();
                if (introHeight < screenHeight) {
                    return false;
                }
                int fringe = introHeight - screenHeight * 3 / 2;
                if (scrollPos > fringe) {
                    long nowTimer = Calendar.getInstance().getTimeInMillis();
                    if (nowTimer - timer < 1000) { // TODO: yeah, baby
                        return false;
                    }
                    timer = nowTimer;
                    Log.i(Loader.getLI(), String.format("Called %d", ltLines.size()));
                    outPartList();
                }
                return false;
            }
        });
        // setAudioListeners(); // Set new audio listeners
        // ------------------------------------------------
        outPartList();
        setEvents();
    }

    private void setEvents() {
        Player p = Player.getInstance();
        p.setCB("play", new Player.CB() {
            @Override
            public void run(int pos) {
                if (pos >= ltLines.size()) {
                    outPartList();
                }
                swPlayPause(pos, true);
            }
        });
        p.setCB("pause", new Player.CB() {
            @Override
            public void run(int pos) {
                swPlayPause(pos, false);
            }
        });
        p.setCB("playError", new Player.CB() {
            @Override
            public void run(int pos) {
                Toast.makeText(
                        AudioActivity.this,
                        "Cannot load audio",
                        Toast.LENGTH_SHORT
                ).show();
                swPlayPause(pos, false);
            }
        });
    }

    private void swPlayPause(int pos, boolean state) {
        if (pos >= ltLines.size()) {
            return;
        }
        ltLines.get(pos)
                .findViewById(R.id.btn_play)
                .setVisibility(state ? View.GONE : View.VISIBLE);
        ltLines.get(pos)
                .findViewById(R.id.btn_pause)
                .setVisibility(state ? View.VISIBLE : View.GONE);
    }

    private void outPartList() {
        int sizePL = Player.getInstance().getAudios().length;
        for (int i = 0; i < 30; i++) {
            int sizeGUI = ltLines.size();
            if (sizeGUI >= sizePL) {
                if (sizeGUI > sizePL) {
                    Log.e(Loader.getLI(), "WTF");
                }
                break;
            }
            newLine();
        }
    }

    private void newLine() {
        // Get current line
        final int posAudio = ltLines.size();
        // Create duplicate
        final TableLayout f = (
                (ScrollView) LayoutInflater
                        .from(this)
                        .inflate(R.layout.activity_audio, null)
        ).findViewById(R.id.audio_list);
        ((LinearLayout) f.getParent()).removeView(f);
        tbLines.addView(f);
        int playingNow = Player.getInstance().getPlaying();
        // Set elements
        f.findViewById(R.id.audio_line).setVisibility(View.VISIBLE);
        f.findViewById(R.id.btn_play).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swPlayPause(posAudio, true);
                        clicker.onPlay(posAudio, false);
                    }
                }
        );
        f.findViewById(R.id.btn_pause).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swPlayPause(posAudio, false);
                        clicker.onPlay(posAudio, true);
                    }
                }
        );
        ((TextView) f.findViewById(R.id.txt_name)).setText(clicker.getTxt(posAudio));
        f.findViewById(R.id.btn_saved).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clicker.onSave(posAudio, !((CheckBox) v).isChecked());
                    }
                }
        );
        ((CheckBox) f.findViewById(R.id.btn_saved))
                .setChecked(clicker.getSaved(posAudio));
        f.findViewById(R.id.btn_menu).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showMenu(
                                clicker.onMenu(posAudio),
                                new Consumer<Integer>() {

                                    @Override
                                    public void accept(Integer posMenu) {
                                        clicker.onMenuClick(posAudio, posMenu);
                                    }
                                },
                                v
                        );
                    }
                }
        );
        ltLines.add(f);
        if (posAudio == playingNow) {
            swPlayPause(posAudio, true);
        }
    }

    private <A> void showMenu(A[] items, final Consumer<Integer> cb, View v) {
        PopupMenu menu = new PopupMenu(this, v);
        for (int i = 0; i < items.length; i++) {
            String item = items[i].toString();
            int sett = 1;
            if (item.substring(0, 1).equals("~")) {
                item = item.substring(1, item.length());
                sett = 0;
            }
            menu.getMenu().add(sett, i, i, item);
            menu.getMenu().getItem(i).setEnabled(sett == 1);
        }
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getGroupId() == 1) {
                    cb.accept(item.getItemId());
                }
                return true;
            }
        });
        menu.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Player.getInstance().resetCB();
    }

    private void showHtml(String title, String body, boolean isPics) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        dlg = builder
                .setTitle(title)
                .setMessage(isPics ? Html.fromHtml(
                        body,
                        new HtmlPicParser(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        dlg.show();
                                    }
                                },
                                300, 300
                        ),
                        null
                ) : Html.fromHtml(body))
                .create();
        if (!isPics) {
            dlg.show();
        }
    }

    private interface LineClicker {
        void onPlay(int pos, boolean state);

        void onSave(int pos, boolean state);

        Object[] onMenu(int pos);

        void onMenuClick(int posAudio, int posMenu);

        String getTxt(int pos);

        boolean getSaved(int pos);
    }

    private class ListClicker implements LineClicker {
        @Override
        public void onPlay(final int pos, boolean state) {
            Log.i(Loader.getLI(), String.format(
                    "At pos %d %s",
                    pos,
                    state ? "stop" : "play"
            ));
            if (state) {
                Player.getInstance().pauseAudio();
            } else {
                Player.getInstance().playAudio(pos);
            }
        }

        @Override
        public void onSave(final int pos, boolean state) {
            Log.i(Loader.getLI(), String.format(
                    "At pos %d %s",
                    pos,
                    state ? "unsaved" : "saved"
            ));
            ltLines.get(pos).findViewById(R.id.btn_saved)
                    .setEnabled(false);
            if (state) {
                new AlertDialog.Builder(AudioActivity.this)
                        .setTitle("Delete")
                        .setMessage("Are you sure?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                new AudioRemover(pos).execute(
                                        Player.getInstance().getAudios()[pos]
                                );
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                CheckBox chb = ltLines.get(pos).findViewById(R.id.btn_saved);
                                chb.setEnabled(true);
                                chb.setChecked(true);
                            }
                        })
                        .show();
            } else {
                new AudioSaver(pos).executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR,
                        Player.getInstance().getAudios()[pos]
                );
            }
        }

        @Override
        public String[] onMenu(int pos) {
            Audio[] audios = Player.getInstance().getAudios();
            String[] res = new String[]{
                    getString(R.string.text_show_lyrics),
                    getString(R.string.text_show_pics),
                    getString(R.string.menu_send_filer)
            };
            if (!audios[pos].hasText()) {
                res[0] = "~" + res[0];
            }
            if (!audios[pos].hasPic()) {
                res[1] = "~" + res[1];
            }
            return res;
        }

        @Override
        public void onMenuClick(int posAudio, int posMenu) {
            Log.i(Loader.getLI(), String.format(
                    "On pos %d %d",
                    posAudio,
                    posMenu
            ));
            switch (posMenu) {
                case 0:
                    new TextLoader().execute(
                            Player.getInstance().getAudios()[posAudio]
                    );
                    break;
                case 1:
                    showHtml(
                            "Pics",
                            Audio.Collector.getPicsAsHtml(
                                    Player.getInstance().getAudios()[posAudio]
                            ), true
                    );
                    break;
                case 2:
                    new FilerSender().execute(
                            Player.getInstance().getAudios()[posAudio]
                    );
                    break;
            }
        }

        @Override
        public String getTxt(int pos) {
            return Player.getInstance().getAudios()[pos].toString();
        }

        @Override
        public boolean getSaved(int pos) {
            return Player.getInstance().getAudios()[pos].isSaved();
        }
    }

    private class AudioSaver extends AsyncTask<Audio, Object, Boolean> {

        private int pos;

        AudioSaver(int pos) {
            this.pos = pos;
        }

        @Override
        protected Boolean doInBackground(Audio... pls) {
            try {
                if (pls[0].hasText() && pls[0].getText() == null) {
                    Player.getInstance().getRequester()
                            .getLyrics(pls[0]);
                }
                if (!Audio.Collector.saveAudio(pls[0])) {
                    publishProgress("Loading url...");
                    Uri url = Player.getInstance()
                            .getRequester().getAudioUri(pls[0]);
                    if (url == null) {
                        publishProgress("Cannot get url");
                        return false;
                    }
                    publishProgress("Saving...");
                    if (!Audio.Collector.saveAudio(pls[0])) {
                        publishProgress("Cannot save");
                        return false;
                    }
                }
                publishProgress("Saved");
                return true;
            } catch (Requester.RequesterError | ParserError | Loader.LoaderError e) {
                publishProgress(e);
            } catch (Throwable e) {
                publishProgress("Other: " + e.getMessage());
            }
            return false;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
            Object res = values[0];
            if (res instanceof String) {
                Log.i(Loader.getLI(), (String) res);
            } else {
                Log.e(Loader.getLI(), ((Throwable) res).getMessage());
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            CheckBox chb = ltLines.get(pos).findViewById(R.id.btn_saved);
            if (!result) {
                Toast.makeText(
                        AudioActivity.this,
                        R.string.err_loading,
                        Toast.LENGTH_SHORT
                ).show();
                chb.setChecked(false);
            }
            chb.setEnabled(true);
        }
    }

    private class AudioRemover extends AsyncTask<Audio, Object, Boolean> {

        private int pos;

        AudioRemover(int pos) {
            this.pos = pos;
        }

        @Override
        protected Boolean doInBackground(Audio... pls) {
            try {
                Audio.Collector.removeAudioDoc(pls[0]);
                publishProgress("Removed");
                return true;
            } catch (Loader.LoaderError e) {
                publishProgress(e);
            } catch (Throwable e) {
                publishProgress("Other: " + e.getMessage());
            }
            return false;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
            Object res = values[0];
            if (res instanceof String) {
                Log.i(Loader.getLI(), (String) res);
            } else {
                Log.e(Loader.getLI(), ((Throwable) res).getMessage());
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            CheckBox chb = ltLines.get(pos).findViewById(R.id.btn_saved);
            if (!result) {
                Toast.makeText(
                        AudioActivity.this,
                        R.string.err_loading,
                        Toast.LENGTH_SHORT
                ).show();
                chb.setChecked(false);
            }
            chb.setEnabled(true);
        }
    }

    private class TextLoader extends AsyncTask<Audio, Object, String> {

        @Override
        protected String doInBackground(Audio... pls) {
            try {
                String res = pls[0].getText();
                if (res == null) {
                    res = Player.getInstance().getRequester()
                            .getLyrics(pls[0]);
                }
                publishProgress("Lyrics loaded");
                return res;
            } catch (Requester.RequesterError | ParserError e) {
                publishProgress(e);
            } catch (Throwable e) {
                publishProgress("Other: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
            Object res = values[0];
            if (res instanceof String) {
                Log.i(Loader.getLI(), (String) res);
            } else {
                Log.e(Loader.getLI(), ((Throwable) res).getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result == null) {
                Toast.makeText(
                        AudioActivity.this,
                        R.string.err_loading,
                        Toast.LENGTH_SHORT
                ).show();
                return;
            }
            showHtml(
                    getResources().getString(R.string.title_lyrics),
                    result,
                    false
            );
        }
    }

    private class FilerSender extends AsyncTask<Audio, Object, Boolean> {

        @Override
        protected Boolean doInBackground(Audio... audios) {
            Audio audio = audios[0];
            try {
                Uri url;
                if (audio.isSaved()) {
                    url = Audio.Collector.getSavedAudio(audio);
                    if (url == null) {
                        publishProgress("Cannot get saved");
                        return false;
                    }
                } else {
                    url = Audio.Collector.getLastURL(audio);
                    if (url == null) {
                        publishProgress("Loading url...");
                        url = Player.getInstance()
                                .getRequester().getAudioUri(audio);
                        if (url == null) {
                            publishProgress("Cannot get url");
                            return false;
                        }
                    }
                }
                publishProgress("Sending to Filer...");
                try {
                    boolean res = FilerIntegration.send(url);
                    publishProgress("Sending to Filer ended");
                    return res;
                } catch (FilerIntegration.FilerError e) {
                    throw new Exception("Filer error");
                }
            } catch (Requester.RequesterError | ParserError | Loader.LoaderError e) {
                publishProgress(e);
            } catch (Throwable e) {
                publishProgress("Other: " + e.getMessage());
            }
            return false;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
            Object res = values[0];
            if (res instanceof String) {
                Log.i(Loader.getLI(), (String) res);
            } else {
                Log.e(Loader.getLI(), ((Throwable) res).getMessage());
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (!result) {
                Toast.makeText(
                        AudioActivity.this,
                        R.string.filer_err,
                        Toast.LENGTH_SHORT
                ).show();
            } else {
                Toast.makeText(
                        AudioActivity.this,
                        R.string.filer_sucs,
                        Toast.LENGTH_SHORT
                ).show();
            }
        }
    }
}
