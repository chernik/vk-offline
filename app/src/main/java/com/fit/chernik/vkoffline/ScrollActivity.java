package com.fit.chernik.vkoffline;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.fit.chernik.vkoffline.Stuff.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ScrollActivity extends AppCompatActivity {
    ImageView img;
    MediaPlayer mp;
    Visualizer visualizer;
    float height = 50;
    float width = 200;
    List<byte[]> times;
    SeekBar slider;
    int dt = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll);
        img = findViewById(R.id.scroll_image);
        slider = findViewById(R.id.scroll_slider);
        slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mp.seekTo(mp.getDuration() * slider.getProgress() / slider.getMax());
            }
        });
        times = new ArrayList<>();
        startListen();
    }

    void drawArr() {
        Bitmap bitmap = Bitmap.createBitmap(500, 300, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(Color.TRANSPARENT);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        for (int i = 0; i < times.size() / dt; i++) {
            paint.setColor(Color.argb(255 * i / times.size() * dt, 255, 0, 0));
            drawOne(times.get(i * dt), canvas, paint, 50 + i * i * 2, i * 10, i * 20);
        }
        canvas.save();
        img.setImageBitmap(bitmap);
    }

    void drawOne(byte[] res, Canvas canvas, Paint paint, float top, float left, float dwidth) {
        int len = res.length * 100 / 150;
        float w = width + dwidth;
        for (int i = 0; i < len; i++) {
            canvas.drawLine(
                    w * i / len + left,
                    top + ((float) Math.exp(3.0 * i / len)) * res[i] * height / 150,
                    w * (i + 1) / len + left,
                    top + ((float) Math.exp(3.0 * (i + 1) / len)) * res[i + 1] * height / 150,
                    paint
            );
        }
    }

    void startListen() {
        mp = Player.getInstance().getMp();
        if (mp == null) {
            finish();
            return;
        }
        visualizer = new Visualizer(mp.getAudioSessionId());
        visualizer.setEnabled(false);
        visualizer.setCaptureSize(128);
        visualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
            @Override
            public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate) {

            }

            @Override
            public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
                MediaPlayer mp2 = Player.getInstance().getMp();
                if (mp2 == null) {
                    return;
                }
                if (mp != mp2) {
                    visualizer.setEnabled(false);
                    visualizer.release();
                    startListen();
                    return;
                }
                times.add(Arrays.copyOf(fft, fft.length));
                if (times.size() > dt * 10) {
                    times.remove(0);
                }
                drawArr();
                updateSlider();
            }
        }, Visualizer.getMaxCaptureRate(), false, true);
        visualizer.setEnabled(true);
    }

    void updateSlider(){
        try {
            slider.setProgress(slider.getMax() * mp.getCurrentPosition() / mp.getDuration());
        } catch(IllegalStateException ignored){

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        visualizer.setEnabled(false);
        visualizer.release();
    }

}
