package com.fit.chernik.vkoffline.Stuff;

import android.net.Uri;

import com.fit.chernik.vkoffline.objects.Audio;
import com.fit.chernik.vkoffline.objects.ParserError;
import com.fit.chernik.vkoffline.objects.Playlist;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class Requester {

    static final private String postUrl = "https://vk.com/al_audio.php";
    static final private int TIMEOUT = 5000;
    private String cookie = null;
    private long userId = 0;

    /**
     * asd => %12%23%34
     *
     * @param str 'asd'
     * @return '%12%23%34'
     */
    static public String encodeURL(String str) {
        try {
            return URLEncoder.encode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            // Returned real string
            return str;
        }
    }

    static public String decodeURL(String str) {
        try {
            return URLDecoder.decode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            // Returned real string
            return str;
        }
    }

    /**
     * Map => queryString
     *
     * @param params Map
     * @return queryString
     */
    private String encodeQuery(Map<String, String> params) {
        StringBuilder sb = new StringBuilder();
        boolean isFirst = true;
        for (String key : params.keySet()) {
            if (!isFirst) {
                sb.append("&");
            } else {
                isFirst = false;
            }
            sb
                    .append(encodeURL(key))
                    .append("=")
                    .append(encodeURL(params.get(key)));
        }
        return sb.toString();
    }

    /**
     * POST request on link {@link Requester#postUrl}
     *
     * @param params queryString body for request as Map
     * @return body of result as String
     * @throws RequesterError any unexpected situation
     */
    private String[] postReq(Map<String, String> params) throws RequesterError {
        if (cookie == null) {
            throw new RequesterError("Null cookie");
        }
        String body = encodeQuery(params);
        HttpsURLConnection req;
        try {
            URL url = new URL(postUrl);
            req = (HttpsURLConnection) url.openConnection();
            req.setRequestMethod("POST");
        } catch (IOException e) {
            throw new RequesterError("Cannot create connection");
        }
        req.setDoOutput(true); // ??
        req.setDoInput(true); // ??
        req.setConnectTimeout(TIMEOUT);
        req.setReadTimeout(TIMEOUT);
        // VK requested cookies
        req.setRequestProperty("Cookie", cookie);
        // for body as queryString
        req.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        OutputStream wd;
        BufferedReader rd;
        try {
            req.connect();
            wd = req.getOutputStream();
        } catch (IOException e) {
            throw new RequesterError("Cannot open connection");
        }
        try {
            wd.write(body.getBytes());
            wd.flush();
            wd.close();
        } catch (IOException e) {
            throw new RequesterError("Cannot write data");
        }
        try {
            rd = new BufferedReader(
                    new InputStreamReader(
                            req.getInputStream(),
                            "windows-1251"
                    )
            );
        } catch (IOException e) {
            throw new RequesterError("Cannot open read");
        }
        StringBuilder res = new StringBuilder();
        try {
            String line;
            while ((line = rd.readLine()) != null) {
                res.append(line).append("\n");
            }
            rd.close();
        } catch (IOException e) {
            throw new RequesterError("Cannot read data");
        }
        // Response format is an array as string with delim '<!>'
        String[] data = res.toString().split("<!--", 2);
        if (data.length == 1) {
            throw new RequesterError("Wrong response format");
        }
        return data;
    }

    /**
     * Get list of playlists as {@link Playlist}
     *
     * @return playlists
     * @throws RequesterError error with connection
     * @throws ParserError    error with parse result
     */
    public Playlist[] getPlaylists() throws RequesterError, ParserError {
        Map<String, String> params = new HashMap<>();
        params.put("act", "section");
        params.put("al", "1");
        params.put("is_layer", "0");
        params.put("owner_id", String.valueOf(userId));
        params.put("section", "playlists");
        String[] data = postReq(params);
        return Playlist.Parser.parse(data);
    }

    /**
     * Get part of list of audios from playlist as {@link Audio}
     *
     * @param playlist playlist
     * @param offset   start index
     * @return audios
     * @throws RequesterError error with connection
     * @throws ParserError    error with parse result
     */
    private Audio[] getAudioPart(Playlist playlist, long offset) throws RequesterError, ParserError {
        Map<String, String> params = playlist.getParamsForReq();
        params.put("act", "load_section");
        params.put("al", "1");
        params.put("claim", "0");
        params.put("offset", String.valueOf(offset));
        params.put("type", "playlist");
        params.put("is_loading_all", "1");
        params.put("from_id", String.valueOf(userId));
        String[] res = postReq(params);
        return Audio.Parser.parse(res, playlist);
    }

    /**
     * Get full list of audios from playlist as {@link Audio}
     *
     * @param playlist playlist
     * @return plst of audios
     * @throws RequesterError error with connection
     * @throws ParserError    error with parse result
     */
    public Audio[] getAudios(Playlist playlist) throws RequesterError, ParserError {
        List<Audio> res = new ArrayList<>();
        Audio[] ret;
        long offset = 0;
        do {
            ret = getAudioPart(playlist, offset);
            offset += ret.length;
            res.addAll(Arrays.asList(ret));
        } while (offset < playlist.getAudioCount());
        if (offset > playlist.getAudioCount()) {
            throw new ParserError("Loaded more audios than can be");
        }
        return res.toArray(new Audio[res.size()]);
    }

    /**
     * Get working URL of audio
     *
     * @param audio audio
     * @return URI
     * @throws ParserError    error with parse result
     * @throws RequesterError error with connection
     */
    public Uri getAudioUri(Audio audio) throws ParserError, RequesterError {
        Map<String, String> params = new HashMap<>();
        params.put("ids", audio.getStrForURL());
        params.put("act", "reload_audio");
        params.put("al", "1");
        String[] data = postReq(params);
        audio.setFromUser(userId);
        return Audio.Parser.attachURL(data, audio);
    }

    /**
     * Set actual cookies and user id for requests
     *
     * @param cookie cookies
     * @param userId user id
     */
    public void setSession(String cookie, long userId) {
        this.cookie = cookie;
        this.userId = userId;
    }

    public String getLyrics(Audio audio) throws RequesterError, ParserError {
        Map<String, String> params = Audio.Collector.getParamsForLyrics(audio);
        params.put("act", "get_lyrics");
        params.put("al", "1");
        params.put("al_ad", "1");
        /*params.put("ads_section", "audio");
        params.put("ads_showed", "5_cc822ac9,5_4a310dec,5_c648e9bd");*/
        String[] res = postReq(params);
        return Audio.Parser.attachLyrics(res, audio);
    }

    public long getUserId() {
        return userId;
    }

    public class RequesterError extends Exception {
        public RequesterError(String s) {
            super(s);
        }
    }
}
