package com.fit.chernik.vkoffline;

import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.fit.chernik.vkoffline.Stuff.Loader;
import com.fit.chernik.vkoffline.Stuff.Player;
import com.fit.chernik.vkoffline.objects.Audio;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    private List<String> nodes = new ArrayList<>();
    private ArrayAdapter<String> nodesAdp;
    private Audio[] audios;
    private List<Integer> selected = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        // -------------------------------------
        nodesAdp = new ArrayAdapter<String>(
                this,
                R.layout.sear_textlist,
                nodes
        );
        audios = Player.getInstance().getAudios();
        if(audios == null){
            Toast.makeText(
                    this,
                    R.string.err_empty_playlist,
                    Toast.LENGTH_SHORT
            ).show();
            retRes(null);
        }
        ((ListView) findViewById(R.id.sear_list))
                .setAdapter(nodesAdp);
        Button but;
        EditText txt;
        // -----------------------------------------
        but = findViewById(R.id.sear_open);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retRes(getSelected());
            }
        });
        // ----------------------------------------
        but = findViewById(R.id.sear_exit);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retRes(null);
            }
        });
        // ----------------------------------------
        txt = findViewById(R.id.sear_text);
        txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                search(String.valueOf(s));
                outList();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void retRes(int[] res){
        Intent intent = new Intent();
        intent.putExtra("sounds", res);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void outList(){
        nodes.clear();
        for(int i = 0; i < selected.size() && i < 5; i++){
            nodes.add(
                    audios[selected.get(i)].toString()
            );
        }
        nodesAdp.notifyDataSetChanged();
    }

    private boolean isMatch(String str, String key){
        return str.toLowerCase().contains(key.toLowerCase());
    }

    private void search(String s){
        selected.clear();
        for(int i = 0; i < audios.length; i++){
            if(isMatch(audios[i].toString(), s)){
                selected.add(i);
            }
        }
    }

    private int[] getSelected(){
        int[] res = new int[selected.size()];
        for(int i = 0; i < res.length; i++){
            res[i] = selected.get(i);
        }
        return res;
    }
}
